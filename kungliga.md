---
title: "CCQ 2018 Figures and Tables: Kungliga"
author: "Lahti, Marjanen, Roivainen, Tolonen"
date: "2018-06-29"
output: html_document
---



# CCQ 2018 / Kungliga




## General 1: Latin share

Annual title count percentage of books in Latin per year (missing
mappings ignored). In Kungliga we now have only primary language
information listed for each document. **No multilingual information in
Kungliga**.

<img src="figures/kungliga_general1_languages-1.png" title="plot of chunk general1_languages" alt="plot of chunk general1_languages" width="500px" />



## Vernacularization

Paper consumption for top languages in the whole catalog.

<img src="figures/kungliga_vernacularization_overall-1.png" title="plot of chunk vernacularization_overall" alt="plot of chunk vernacularization_overall" width="500px" />



## Vernacularizations by city

Annual paper consumption for different (primary) languages in selected places.

![plot of chunk vernacular_by_city](figures/kungliga_vernacular_by_city-1.png)


## Octavo 

Annual paper consumption for different book formats: total and relative.

<img src="figures/kungliga_octavo-1.png" title="plot of chunk octavo" alt="plot of chunk octavo" width="550px" /><img src="figures/kungliga_octavo-2.png" title="plot of chunk octavo" alt="plot of chunk octavo" width="550px" />




## Octavo: paper share by place

Paper share per year per place.

![plot of chunk octavo2](figures/kungliga_octavo2-1.png)





## Combined 1

Shares of different book formats (based on title count!) in different
languages for 50-year periods.

![plot of chunk combined1](figures/kungliga_combined1-1.png)


The exact numbers are shown in the table. **The percentage
calculations ignore missing values at the moment (OK?)**:


|City       |Language     |publication_period |Format    |    N| Fraction (%)|
|:----------|:------------|:------------------|:---------|----:|------------:|
|Linköping  |Latin        |1600-1649          |Quarto    |    1|          8.3|
|Linköping  |Latin        |1600-1649          |Octavo    |    9|         75.0|
|Linköping  |Latin        |1600-1649          |Folio     |    2|         16.7|
|Linköping  |Swedish      |1600-1649          |Quarto    |   18|         72.0|
|Linköping  |Swedish      |1600-1649          |Octavo    |    7|         28.0|
|Linköping  |Undetermined |1600-1649          |Quarto    |    3|        100.0|
|Stockholm  |German       |1600-1649          |Quarto    |   26|         32.9|
|Stockholm  |German       |1600-1649          |Octavo    |   21|         26.6|
|Stockholm  |German       |1600-1649          |Folio     |   30|         38.0|
|Stockholm  |German       |1600-1649          |Duodecimo |    2|          2.5|
|Stockholm  |Latin        |1600-1649          |Quarto    |   77|         38.1|
|Stockholm  |Latin        |1600-1649          |Octavo    |   86|         42.6|
|Stockholm  |Latin        |1600-1649          |Folio     |   22|         10.9|
|Stockholm  |Latin        |1600-1649          |Duodecimo |   17|          8.4|
|Stockholm  |Swedish      |1600-1649          |Quarto    |  539|         49.4|
|Stockholm  |Swedish      |1600-1649          |Octavo    |  206|         18.9|
|Stockholm  |Swedish      |1600-1649          |Folio     |  323|         29.6|
|Stockholm  |Swedish      |1600-1649          |Duodecimo |   22|          2.0|
|Gothenburg |German       |1650-1699          |Quarto    |    3|         50.0|
|Gothenburg |German       |1650-1699          |Octavo    |    2|         33.3|
|Gothenburg |German       |1650-1699          |Folio     |    1|         16.7|
|Gothenburg |Latin        |1650-1699          |Quarto    |    2|         10.0|
|Gothenburg |Latin        |1650-1699          |Octavo    |   12|         60.0|
|Gothenburg |Latin        |1650-1699          |Duodecimo |    6|         30.0|
|Gothenburg |Swedish      |1650-1699          |Quarto    |   46|         47.9|
|Gothenburg |Swedish      |1650-1699          |Octavo    |   24|         25.0|
|Gothenburg |Swedish      |1650-1699          |Duodecimo |   26|         27.1|
|Linköping  |Latin        |1650-1699          |Quarto    |    1|          2.6|
|Linköping  |Latin        |1650-1699          |Octavo    |   35|         89.7|
|Linköping  |Latin        |1650-1699          |Folio     |    1|          2.6|
|Linköping  |Latin        |1650-1699          |Duodecimo |    2|          5.1|
|Linköping  |Swedish      |1650-1699          |Quarto    |   52|         68.4|
|Linköping  |Swedish      |1650-1699          |Octavo    |   18|         23.7|
|Linköping  |Swedish      |1650-1699          |Folio     |    1|          1.3|
|Linköping  |Swedish      |1650-1699          |Duodecimo |    5|          6.6|
|Linköping  |Undetermined |1650-1699          |Quarto    |    1|        100.0|
|Lund       |Latin        |1650-1699          |Quarto    |    5|         50.0|
|Lund       |Latin        |1650-1699          |Octavo    |    2|         20.0|
|Lund       |Latin        |1650-1699          |Folio     |    3|         30.0|
|Lund       |Swedish      |1650-1699          |Quarto    |    6|         60.0|
|Lund       |Swedish      |1650-1699          |Octavo    |    4|         40.0|
|Stockholm  |German       |1650-1699          |Quarto    |   56|         47.9|
|Stockholm  |German       |1650-1699          |Octavo    |   16|         13.7|
|Stockholm  |German       |1650-1699          |Folio     |   25|         21.4|
|Stockholm  |German       |1650-1699          |Duodecimo |   20|         17.1|
|Stockholm  |Latin        |1650-1699          |Quarto    |   40|         25.0|
|Stockholm  |Latin        |1650-1699          |Octavo    |   74|         46.2|
|Stockholm  |Latin        |1650-1699          |Folio     |   27|         16.9|
|Stockholm  |Latin        |1650-1699          |Duodecimo |   19|         11.9|
|Stockholm  |Swedish      |1650-1699          |Quarto    | 1734|         72.7|
|Stockholm  |Swedish      |1650-1699          |Octavo    |  183|          7.7|
|Stockholm  |Swedish      |1650-1699          |Folio     |  316|         13.3|
|Stockholm  |Swedish      |1650-1699          |Duodecimo |  151|          6.3|
|Gothenburg |German       |1700-1749          |Quarto    |    9|         50.0|
|Gothenburg |German       |1700-1749          |Octavo    |    2|         11.1|
|Gothenburg |German       |1700-1749          |Folio     |    7|         38.9|
|Gothenburg |Latin        |1700-1749          |Quarto    |    4|         12.1|
|Gothenburg |Latin        |1700-1749          |Octavo    |   17|         51.5|
|Gothenburg |Latin        |1700-1749          |Folio     |    8|         24.2|
|Gothenburg |Latin        |1700-1749          |Duodecimo |    4|         12.1|
|Gothenburg |Swedish      |1700-1749          |Quarto    |   55|         40.4|
|Gothenburg |Swedish      |1700-1749          |Octavo    |   34|         25.0|
|Gothenburg |Swedish      |1700-1749          |Folio     |   21|         15.4|
|Gothenburg |Swedish      |1700-1749          |Duodecimo |   26|         19.1|
|Linköping  |Latin        |1700-1749          |Quarto    |   10|         25.6|
|Linköping  |Latin        |1700-1749          |Octavo    |   28|         71.8|
|Linköping  |Latin        |1700-1749          |Folio     |    1|          2.6|
|Linköping  |Swedish      |1700-1749          |Quarto    |   71|         74.0|
|Linköping  |Swedish      |1700-1749          |Octavo    |   16|         16.7|
|Linköping  |Swedish      |1700-1749          |Folio     |    2|          2.1|
|Linköping  |Swedish      |1700-1749          |Duodecimo |    7|          7.3|
|Lund       |German       |1700-1749          |Quarto    |    6|         42.9|
|Lund       |German       |1700-1749          |Octavo    |    5|         35.7|
|Lund       |German       |1700-1749          |Folio     |    3|         21.4|
|Lund       |Latin        |1700-1749          |Quarto    |   47|         49.0|
|Lund       |Latin        |1700-1749          |Octavo    |   20|         20.8|
|Lund       |Latin        |1700-1749          |Folio     |   28|         29.2|
|Lund       |Latin        |1700-1749          |Duodecimo |    1|          1.0|
|Lund       |Swedish      |1700-1749          |Quarto    |  101|         61.6|
|Lund       |Swedish      |1700-1749          |Octavo    |   17|         10.4|
|Lund       |Swedish      |1700-1749          |Folio     |   32|         19.5|
|Lund       |Swedish      |1700-1749          |Duodecimo |   14|          8.5|
|Stockholm  |German       |1700-1749          |Quarto    |  173|         58.6|
|Stockholm  |German       |1700-1749          |Octavo    |   30|         10.2|
|Stockholm  |German       |1700-1749          |Folio     |   80|         27.1|
|Stockholm  |German       |1700-1749          |Duodecimo |   12|          4.1|
|Stockholm  |Latin        |1700-1749          |Quarto    |  139|         37.0|
|Stockholm  |Latin        |1700-1749          |Octavo    |  133|         35.4|
|Stockholm  |Latin        |1700-1749          |Folio     |   91|         24.2|
|Stockholm  |Latin        |1700-1749          |Duodecimo |   13|          3.5|
|Stockholm  |Swedish      |1700-1749          |Quarto    | 3725|         72.7|
|Stockholm  |Swedish      |1700-1749          |Octavo    |  806|         15.7|
|Stockholm  |Swedish      |1700-1749          |Folio     |  391|          7.6|
|Stockholm  |Swedish      |1700-1749          |Duodecimo |  201|          3.9|
|Gothenburg |German       |1750-1799          |Quarto    |    1|         33.3|
|Gothenburg |German       |1750-1799          |Octavo    |    1|         33.3|
|Gothenburg |German       |1750-1799          |Duodecimo |    1|         33.3|
|Gothenburg |Latin        |1750-1799          |Quarto    |    8|         29.6|
|Gothenburg |Latin        |1750-1799          |Octavo    |   19|         70.4|
|Gothenburg |Swedish      |1750-1799          |Quarto    |  254|         35.7|
|Gothenburg |Swedish      |1750-1799          |Octavo    |  395|         55.5|
|Gothenburg |Swedish      |1750-1799          |Folio     |    5|          0.7|
|Gothenburg |Swedish      |1750-1799          |Duodecimo |   58|          8.1|
|Linköping  |Latin        |1750-1799          |Quarto    |   15|         40.5|
|Linköping  |Latin        |1750-1799          |Octavo    |   16|         43.2|
|Linköping  |Latin        |1750-1799          |Folio     |    6|         16.2|
|Linköping  |Swedish      |1750-1799          |Quarto    |  155|         47.7|
|Linköping  |Swedish      |1750-1799          |Octavo    |  145|         44.6|
|Linköping  |Swedish      |1750-1799          |Folio     |   13|          4.0|
|Linköping  |Swedish      |1750-1799          |Duodecimo |   12|          3.7|
|Lund       |German       |1750-1799          |Octavo    |    3|        100.0|
|Lund       |Latin        |1750-1799          |Quarto    |   97|         78.2|
|Lund       |Latin        |1750-1799          |Octavo    |   18|         14.5|
|Lund       |Latin        |1750-1799          |Folio     |    9|          7.3|
|Lund       |Swedish      |1750-1799          |Quarto    |  187|         37.0|
|Lund       |Swedish      |1750-1799          |Octavo    |  276|         54.7|
|Lund       |Swedish      |1750-1799          |Folio     |   28|          5.5|
|Lund       |Swedish      |1750-1799          |Duodecimo |   14|          2.8|
|Stockholm  |German       |1750-1799          |Quarto    |   39|         47.0|
|Stockholm  |German       |1750-1799          |Octavo    |   39|         47.0|
|Stockholm  |German       |1750-1799          |Folio     |    3|          3.6|
|Stockholm  |German       |1750-1799          |Duodecimo |    2|          2.4|
|Stockholm  |Latin        |1750-1799          |Quarto    |   57|         37.0|
|Stockholm  |Latin        |1750-1799          |Octavo    |   84|         54.5|
|Stockholm  |Latin        |1750-1799          |Folio     |   10|          6.5|
|Stockholm  |Latin        |1750-1799          |Duodecimo |    3|          1.9|
|Stockholm  |Swedish      |1750-1799          |Quarto    | 8778|         64.5|
|Stockholm  |Swedish      |1750-1799          |Octavo    | 4053|         29.8|
|Stockholm  |Swedish      |1750-1799          |Folio     |  370|          2.7|
|Stockholm  |Swedish      |1750-1799          |Duodecimo |  406|          3.0|


