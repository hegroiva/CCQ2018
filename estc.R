# Read the preprocessed data
# git clone git@version.aalto.fi:comhis/estc.git
datafile <- "data_estc/unified/polished/df.Rds"
df0 <- readRDS(datafile)

# Set time period
df0 <- df0 %>% filter(publication_year >= min.year &
                      publication_year <= max.year)

#render("estc.Rmd")
knit("estc.Rmd")


