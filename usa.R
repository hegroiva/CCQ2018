#  USA case ESTC: Yhteensä 9 kuvaa,
#  3 per kieli (englanti, latina sekä muut)
#  3 aluetta (Lontoo, Muu UK, Pohjois-Amerikka)
# primary_language considered

pics <- list()
places <- c("London", "Other England", "USA")
langs <- c("English", "Latin", "Other")

df <- df0 %>% filter(!is.na(gatherings))
df$gatherings <- as.character(df$gatherings)
df$gatherings[which(!df$gatherings %in% top.gatherings)] <- "Other"
df$gatherings <- factor(df$gatherings, levels = c(top.gatherings, "Other"))
df <- df %>% filter(publication_place %in% c("London") |
                     (publication_country == "England" & !publication_place == "London") |
		     publication_country == "USA")
df$publication_place <- as.character(df$publication_place)
df$publication_place[which(df$publication_country == "England" & !df$publication_place == "London")] <- "Other England"
df$publication_place <- factor(df$publication_place, levels = c("London", "Other England", "USA"))

df$language_primary <- as.character(df$language_primary)
df$language_primary[which(!df$language_primary %in% c("English", "Latin"))] <- "Other"
df$language_primary <- factor(df$language_primary, levels = langs)

dfs <- df %>% filter(place %in% places & language_primary %in% langs)

for (place in places) {
  for (lang in langs) {  

    df <- dfs %>% filter(!is.na(publication_decade)) %>%
      	          filter(publication_place == place) %>%
      	          filter(language_primary == lang) %>%		
                  select(gatherings, publication_decade) %>%
      	          group_by(publication_decade, gatherings) %>%
	          tally() %>%
	          group_by(publication_decade) %>%
	          mutate(pct = n/sum(n)) %>%
	          arrange(publication_decade)

    p <- ggplot(df, aes(x = publication_decade, y = pct,
                    color = gatherings,
		    fill = gatherings)) +
       geom_point() +
       geom_smooth() +
       scale_y_continuous(label = scales::percent, limits = c(0, 1)) +         
       labs(x = "Publication decade", y = "Fraction (%)", title = paste(place, lang, sep = "/")) +
       my_color_scale() +
       my_fill_scale() +        
       guides(color=guide_legend(title="Gatherings"),
                   fill = FALSE)
		   
    pics[[paste(place, lang, sep = "/")]] <- p

  }
}

grid.arrange(pics[[1]], pics[[2]], pics[[3]],
             pics[[4]], pics[[5]], pics[[6]],
             pics[[7]], pics[[8]], pics[[9]],
	     nrow = 3)

