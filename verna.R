
df <- df0 %>% filter(!is.na(paper)) %>%
              filter(publication_place %in% cities) %>%
	      select(publication_year, publication_place, languages, paper) %>%
              group_by(publication_year, publication_place, languages) %>%
	      summarise(paper.total = sum(paper, na.rm = TRUE)) %>%
              group_by(publication_year, publication_place) %>%	      
	      mutate(pct.paper = paper.total/sum(paper.total, na.rm = TRUE)) %>%
	      filter(pct.paper > 0) %>%
	      arrange(publication_year)

source("funcs.R")
df <- top_languages_per_place(df, field = "languages", n = ntoplang)
df$publication_place <- droplevels(df$publication_place)
df$pct <- df$pct.paper

p <- ggplot(df, aes(x = publication_year, y = pct,
                    color = languages)) +
		    geom_point() +
  #scale_color_manual(values = palette(rainbow(length(unique(df$languages))))) +
  scale_color_manual(values = palette(rainbow(14))) +  
       guides(color=guide_legend(title="Language"), fill=FALSE)

library(cowplot)
#legend <- get_legend(p)

# Cross plot on publication year vs. page counts in different languages
pics <- list()
for (place in sort(unique(df$publication_place))) {
  dfs <- df %>% filter(publication_place == place) %>%
                filter(pct > 0)
  dfs$languages <- droplevels(dfs$languages)
  p <- ggplot(dfs, aes(x = publication_year, y = pct,
                    color = languages)) +
       geom_point() +
       geom_smooth() +
       #scale_y_continuous(limits = c(0,1), breaks = 10^(0:round(log10(max(df$pct))))) +
       scale_y_continuous(limits = c(0,1), label = scales::percent) +         
       labs(x = "Publication year", y = "Fraction (%)", title = place) +
       guides(color=guide_legend(title="Language"), fill=FALSE)              
       # guides(color=FALSE, fill=FALSE)
  pics[[place]] <- p
}

prow <-  plot_grid(
  pics[[1]], pics[[2]], pics[[3]], pics[[4]], pics[[5]],
  pics[[6]], pics[[7]], pics[[8]], pics[[9]], pics[[10]],
  pics[[11]], 
  nrow = 4)

p <- prow
#p <- plot_grid(prow, legend, rel_widths = c(21, 4))
print(p)

