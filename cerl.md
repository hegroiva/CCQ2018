---
title: "CCQ 2018 Figures and Tables: CERL"
author: "Lahti, Marjanen, Roivainen, Tolonen"
date: "2018-06-29"
output: md_document
---


# CCQ 2018 / CERL



**In all analyses including cities we have taken the top-25 cities from the period 1700-1799.**

Multilingual documents that include Swedish and Latin are listed in
the file [multilingual.csv](multilingual.csv).

<img src="figures/cerl_multilingual-1.png" title="plot of chunk multilingual" alt="plot of chunk multilingual" width="500px" />



## General 1: Latin share

Annual title count percentage of books in Latin per year (missing
mappings and other languages ignored).

![plot of chunk general1_languages](figures/cerl_general1_languages-1.png)


## Octavo 

Annual paper consumption for different book formats: total and relative.

<img src="figures/cerl_octavo-1.png" title="plot of chunk octavo" alt="plot of chunk octavo" width="550px" /><img src="figures/cerl_octavo-2.png" title="plot of chunk octavo" alt="plot of chunk octavo" width="550px" />


## Octavo: paper share by place

Paper share for gatherings per year/place.

![plot of chunk gatherings_paper_by_place](figures/cerl_gatherings_paper_by_place-1.png)




## Vernacularization

Paper consumption in the top languages in the whole catalog.

![plot of chunk vernacularization_overall](figures/cerl_vernacularization_overall-1.png)



## Vernacularizations by city

Annual share of languages in selected cities.

![plot of chunk vernacular_by_city](figures/cerl_vernacular_by_city-1.png)






## Combined / Gatherings

Shares of different book formats (based on title count!) in different
languages.

![plot of chunk combined2](figures/cerl_combined2-1.png)

The exact numbers are shown in the table:


|City       |Language      |publication_period |Format    |     N| Fraction (%)|
|:----------|:-------------|:------------------|:---------|-----:|------------:|
|Amsterdam  |Dutch         |1600-1649          |Quarto    |   201|         42.9|
|Amsterdam  |Dutch         |1600-1649          |Octavo    |   174|         37.1|
|Amsterdam  |Dutch         |1600-1649          |Folio     |    75|         16.0|
|Amsterdam  |Dutch         |1600-1649          |Duodecimo |    19|          4.1|
|Amsterdam  |French        |1600-1649          |Quarto    |    19|         16.5|
|Amsterdam  |French        |1600-1649          |Octavo    |    23|         20.0|
|Amsterdam  |French        |1600-1649          |Folio     |    52|         45.2|
|Amsterdam  |French        |1600-1649          |Duodecimo |    21|         18.3|
|Amsterdam  |Latin         |1600-1649          |Quarto    |   206|         24.2|
|Amsterdam  |Latin         |1600-1649          |Octavo    |   247|         29.0|
|Amsterdam  |Latin         |1600-1649          |Folio     |   151|         17.7|
|Amsterdam  |Latin         |1600-1649          |Duodecimo |   249|         29.2|
|Berlin     |German        |1600-1649          |Quarto    |   238|         85.0|
|Berlin     |German        |1600-1649          |Octavo    |    37|         13.2|
|Berlin     |German        |1600-1649          |Folio     |     1|          0.4|
|Berlin     |German        |1600-1649          |Duodecimo |     4|          1.4|
|Berlin     |Latin         |1600-1649          |Quarto    |    38|         44.2|
|Berlin     |Latin         |1600-1649          |Octavo    |    36|         41.9|
|Berlin     |Latin         |1600-1649          |Folio     |     9|         10.5|
|Berlin     |Latin         |1600-1649          |Duodecimo |     3|          3.5|
|Brussels   |French        |1600-1649          |Quarto    |     7|         31.8|
|Brussels   |French        |1600-1649          |Octavo    |    12|         54.5|
|Brussels   |French        |1600-1649          |Folio     |     2|          9.1|
|Brussels   |French        |1600-1649          |Duodecimo |     1|          4.5|
|Brussels   |Latin         |1600-1649          |Quarto    |     7|         38.9|
|Brussels   |Latin         |1600-1649          |Octavo    |     6|         33.3|
|Brussels   |Latin         |1600-1649          |Folio     |     5|         27.8|
|Brussels   |Undetermined  |1600-1649          |Quarto    |  1131|         79.4|
|Brussels   |Undetermined  |1600-1649          |Octavo    |   170|         11.9|
|Brussels   |Undetermined  |1600-1649          |Folio     |    61|          4.3|
|Brussels   |Undetermined  |1600-1649          |Duodecimo |    62|          4.4|
|Edinburgh  |English       |1600-1649          |Quarto    |   182|         57.4|
|Edinburgh  |English       |1600-1649          |Octavo    |    56|         17.7|
|Edinburgh  |English       |1600-1649          |Folio     |    66|         20.8|
|Edinburgh  |English       |1600-1649          |Duodecimo |    13|          4.1|
|Edinburgh  |Latin         |1600-1649          |Quarto    |    31|         59.6|
|Edinburgh  |Latin         |1600-1649          |Octavo    |    16|         30.8|
|Edinburgh  |Latin         |1600-1649          |Folio     |     4|          7.7|
|Edinburgh  |Latin         |1600-1649          |Duodecimo |     1|          1.9|
|Edinburgh  |Undetermined  |1600-1649          |Quarto    |     8|         57.1|
|Edinburgh  |Undetermined  |1600-1649          |Octavo    |     3|         21.4|
|Edinburgh  |Undetermined  |1600-1649          |Folio     |     3|         21.4|
|Frankfurt  |German        |1600-1649          |Quarto    |  1124|         54.4|
|Frankfurt  |German        |1600-1649          |Octavo    |   477|         23.1|
|Frankfurt  |German        |1600-1649          |Folio     |   376|         18.2|
|Frankfurt  |German        |1600-1649          |Duodecimo |    90|          4.4|
|Frankfurt  |Latin         |1600-1649          |Quarto    |  1780|         33.9|
|Frankfurt  |Latin         |1600-1649          |Octavo    |  1769|         33.7|
|Frankfurt  |Latin         |1600-1649          |Folio     |  1433|         27.3|
|Frankfurt  |Latin         |1600-1649          |Duodecimo |   271|          5.2|
|Frankfurt  |Undetermined  |1600-1649          |Quarto    |    46|         30.5|
|Frankfurt  |Undetermined  |1600-1649          |Octavo    |    25|         16.6|
|Frankfurt  |Undetermined  |1600-1649          |Folio     |    69|         45.7|
|Frankfurt  |Undetermined  |1600-1649          |Duodecimo |    11|          7.3|
|Halle      |German        |1600-1649          |Quarto    |   300|         80.4|
|Halle      |German        |1600-1649          |Octavo    |    62|         16.6|
|Halle      |German        |1600-1649          |Folio     |     5|          1.3|
|Halle      |German        |1600-1649          |Duodecimo |     6|          1.6|
|Halle      |Latin         |1600-1649          |Quarto    |   169|         84.1|
|Halle      |Latin         |1600-1649          |Octavo    |    26|         12.9|
|Halle      |Latin         |1600-1649          |Folio     |     1|          0.5|
|Halle      |Latin         |1600-1649          |Duodecimo |     5|          2.5|
|Halle      |Latin;German  |1600-1649          |Quarto    |    12|         85.7|
|Halle      |Latin;German  |1600-1649          |Octavo    |     2|         14.3|
|Hamburg    |French        |1600-1649          |Quarto    |     1|         25.0|
|Hamburg    |French        |1600-1649          |Octavo    |     3|         75.0|
|Hamburg    |German        |1600-1649          |Quarto    |   476|         65.0|
|Hamburg    |German        |1600-1649          |Octavo    |   199|         27.2|
|Hamburg    |German        |1600-1649          |Folio     |    23|          3.1|
|Hamburg    |German        |1600-1649          |Duodecimo |    34|          4.6|
|Hamburg    |Latin         |1600-1649          |Quarto    |   318|         52.4|
|Hamburg    |Latin         |1600-1649          |Octavo    |   214|         35.3|
|Hamburg    |Latin         |1600-1649          |Folio     |    49|          8.1|
|Hamburg    |Latin         |1600-1649          |Duodecimo |    26|          4.3|
|Jena       |German        |1600-1649          |Quarto    |   271|         82.9|
|Jena       |German        |1600-1649          |Octavo    |    33|         10.1|
|Jena       |German        |1600-1649          |Folio     |    14|          4.3|
|Jena       |German        |1600-1649          |Duodecimo |     9|          2.8|
|Jena       |Latin         |1600-1649          |Quarto    |   768|         76.6|
|Jena       |Latin         |1600-1649          |Octavo    |   199|         19.9|
|Jena       |Latin         |1600-1649          |Folio     |     7|          0.7|
|Jena       |Latin         |1600-1649          |Duodecimo |    28|          2.8|
|Jena       |Latin;German  |1600-1649          |Quarto    |    32|         97.0|
|Jena       |Latin;German  |1600-1649          |Octavo    |     1|          3.0|
|Leiden     |French        |1600-1649          |Quarto    |    17|         17.0|
|Leiden     |French        |1600-1649          |Octavo    |    35|         35.0|
|Leiden     |French        |1600-1649          |Folio     |    26|         26.0|
|Leiden     |French        |1600-1649          |Duodecimo |    22|         22.0|
|Leiden     |Latin         |1600-1649          |Quarto    |   299|         26.2|
|Leiden     |Latin         |1600-1649          |Octavo    |   482|         42.2|
|Leiden     |Latin         |1600-1649          |Folio     |    86|          7.5|
|Leiden     |Latin         |1600-1649          |Duodecimo |   276|         24.1|
|Leiden     |Undetermined  |1600-1649          |Quarto    |    81|         48.2|
|Leiden     |Undetermined  |1600-1649          |Octavo    |    36|         21.4|
|Leiden     |Undetermined  |1600-1649          |Folio     |    12|          7.1|
|Leiden     |Undetermined  |1600-1649          |Duodecimo |    39|         23.2|
|Leipzig    |German        |1600-1649          |Quarto    |  1948|         69.0|
|Leipzig    |German        |1600-1649          |Octavo    |   694|         24.6|
|Leipzig    |German        |1600-1649          |Folio     |   121|          4.3|
|Leipzig    |German        |1600-1649          |Duodecimo |    62|          2.2|
|Leipzig    |Latin         |1600-1649          |Quarto    |  1116|         58.4|
|Leipzig    |Latin         |1600-1649          |Octavo    |   628|         32.8|
|Leipzig    |Latin         |1600-1649          |Folio     |    97|          5.1|
|Leipzig    |Latin         |1600-1649          |Duodecimo |    71|          3.7|
|Leipzig    |Undetermined  |1600-1649          |Quarto    |     5|         41.7|
|Leipzig    |Undetermined  |1600-1649          |Octavo    |     3|         25.0|
|Leipzig    |Undetermined  |1600-1649          |Folio     |     3|         25.0|
|Leipzig    |Undetermined  |1600-1649          |Duodecimo |     1|          8.3|
|London     |English       |1600-1649          |Quarto    |  2976|         58.1|
|London     |English       |1600-1649          |Octavo    |   708|         13.8|
|London     |English       |1600-1649          |Folio     |  1248|         24.4|
|London     |English       |1600-1649          |Duodecimo |   191|          3.7|
|London     |Latin         |1600-1649          |Quarto    |   153|         32.3|
|London     |Latin         |1600-1649          |Octavo    |   148|         31.3|
|London     |Latin         |1600-1649          |Folio     |   135|         28.5|
|London     |Latin         |1600-1649          |Duodecimo |    37|          7.8|
|London     |Undetermined  |1600-1649          |Quarto    |   671|         60.0|
|London     |Undetermined  |1600-1649          |Octavo    |   142|         12.7|
|London     |Undetermined  |1600-1649          |Folio     |   271|         24.2|
|London     |Undetermined  |1600-1649          |Duodecimo |    34|          3.0|
|Madrid     |Latin         |1600-1649          |Quarto    |     3|         20.0|
|Madrid     |Latin         |1600-1649          |Octavo    |     1|          6.7|
|Madrid     |Latin         |1600-1649          |Folio     |    11|         73.3|
|Madrid     |Panjabi       |1600-1649          |Quarto    |    33|         38.4|
|Madrid     |Panjabi       |1600-1649          |Octavo    |    11|         12.8|
|Madrid     |Panjabi       |1600-1649          |Folio     |    42|         48.8|
|Madrid     |Spanish       |1600-1649          |Quarto    |    93|         14.8|
|Madrid     |Spanish       |1600-1649          |Octavo    |    76|         12.1|
|Madrid     |Spanish       |1600-1649          |Folio     |   457|         72.7|
|Madrid     |Spanish       |1600-1649          |Duodecimo |     3|          0.5|
|Nürnberg   |German        |1600-1649          |Quarto    |   424|         52.9|
|Nürnberg   |German        |1600-1649          |Octavo    |   266|         33.2|
|Nürnberg   |German        |1600-1649          |Folio     |    51|          6.4|
|Nürnberg   |German        |1600-1649          |Duodecimo |    61|          7.6|
|Nürnberg   |Latin         |1600-1649          |Quarto    |   357|         53.2|
|Nürnberg   |Latin         |1600-1649          |Octavo    |   250|         37.3|
|Nürnberg   |Latin         |1600-1649          |Folio     |    17|          2.5|
|Nürnberg   |Latin         |1600-1649          |Duodecimo |    47|          7.0|
|Nürnberg   |Undetermined  |1600-1649          |Quarto    |     9|         56.2|
|Nürnberg   |Undetermined  |1600-1649          |Octavo    |     2|         12.5|
|Nürnberg   |Undetermined  |1600-1649          |Folio     |     3|         18.8|
|Nürnberg   |Undetermined  |1600-1649          |Duodecimo |     2|         12.5|
|Paris      |French        |1600-1649          |Quarto    |  1289|         25.9|
|Paris      |French        |1600-1649          |Octavo    |  2974|         59.8|
|Paris      |French        |1600-1649          |Folio     |   556|         11.2|
|Paris      |French        |1600-1649          |Duodecimo |   154|          3.1|
|Paris      |Latin         |1600-1649          |Quarto    |   436|         23.1|
|Paris      |Latin         |1600-1649          |Octavo    |   537|         28.4|
|Paris      |Latin         |1600-1649          |Folio     |   846|         44.8|
|Paris      |Latin         |1600-1649          |Duodecimo |    69|          3.7|
|Paris      |Undetermined  |1600-1649          |Quarto    |   177|         32.2|
|Paris      |Undetermined  |1600-1649          |Octavo    |   146|         26.5|
|Paris      |Undetermined  |1600-1649          |Folio     |   192|         34.9|
|Paris      |Undetermined  |1600-1649          |Duodecimo |    35|          6.4|
|Prague     |Czech         |1600-1649          |Quarto    |     8|         26.7|
|Prague     |Czech         |1600-1649          |Octavo    |     7|         23.3|
|Prague     |Czech         |1600-1649          |Folio     |    12|         40.0|
|Prague     |Czech         |1600-1649          |Duodecimo |     3|         10.0|
|Prague     |German        |1600-1649          |Quarto    |   223|         85.4|
|Prague     |German        |1600-1649          |Octavo    |    30|         11.5|
|Prague     |German        |1600-1649          |Folio     |     6|          2.3|
|Prague     |German        |1600-1649          |Duodecimo |     2|          0.8|
|Prague     |Latin         |1600-1649          |Quarto    |    65|         55.6|
|Prague     |Latin         |1600-1649          |Octavo    |    33|         28.2|
|Prague     |Latin         |1600-1649          |Folio     |    14|         12.0|
|Prague     |Latin         |1600-1649          |Duodecimo |     5|          4.3|
|Rome       |Italian       |1600-1649          |Quarto    |   125|         31.9|
|Rome       |Italian       |1600-1649          |Octavo    |   137|         34.9|
|Rome       |Italian       |1600-1649          |Folio     |    96|         24.5|
|Rome       |Italian       |1600-1649          |Duodecimo |    34|          8.7|
|Rome       |Italian;Latin |1600-1649          |Quarto    |     9|         30.0|
|Rome       |Italian;Latin |1600-1649          |Octavo    |     6|         20.0|
|Rome       |Italian;Latin |1600-1649          |Folio     |    15|         50.0|
|Rome       |Latin         |1600-1649          |Quarto    |   256|         31.7|
|Rome       |Latin         |1600-1649          |Octavo    |   139|         17.2|
|Rome       |Latin         |1600-1649          |Folio     |   386|         47.8|
|Rome       |Latin         |1600-1649          |Duodecimo |    27|          3.3|
|Stockholm  |Finnish       |1600-1649          |Quarto    |    10|         17.5|
|Stockholm  |Finnish       |1600-1649          |Octavo    |    25|         43.9|
|Stockholm  |Finnish       |1600-1649          |Folio     |     5|          8.8|
|Stockholm  |Finnish       |1600-1649          |Duodecimo |    17|         29.8|
|Stockholm  |Latin         |1600-1649          |Quarto    |   149|         44.6|
|Stockholm  |Latin         |1600-1649          |Octavo    |   122|         36.5|
|Stockholm  |Latin         |1600-1649          |Folio     |    47|         14.1|
|Stockholm  |Latin         |1600-1649          |Duodecimo |    16|          4.8|
|Stockholm  |Swedish       |1600-1649          |Quarto    |   526|         39.7|
|Stockholm  |Swedish       |1600-1649          |Octavo    |   276|         20.8|
|Stockholm  |Swedish       |1600-1649          |Folio     |   492|         37.2|
|Stockholm  |Swedish       |1600-1649          |Duodecimo |    30|          2.3|
|Strasbourg |French        |1600-1649          |Quarto    |     1|          7.1|
|Strasbourg |French        |1600-1649          |Octavo    |     9|         64.3|
|Strasbourg |French        |1600-1649          |Folio     |     4|         28.6|
|Strasbourg |German        |1600-1649          |Quarto    |   216|         33.7|
|Strasbourg |German        |1600-1649          |Octavo    |   265|         41.3|
|Strasbourg |German        |1600-1649          |Folio     |   138|         21.5|
|Strasbourg |German        |1600-1649          |Duodecimo |    22|          3.4|
|Strasbourg |Latin         |1600-1649          |Quarto    |  1115|         57.3|
|Strasbourg |Latin         |1600-1649          |Octavo    |   512|         26.3|
|Strasbourg |Latin         |1600-1649          |Folio     |   169|          8.7|
|Strasbourg |Latin         |1600-1649          |Duodecimo |   150|          7.7|
|The Hague  |French        |1600-1649          |Quarto    |     6|         14.3|
|The Hague  |French        |1600-1649          |Octavo    |    10|         23.8|
|The Hague  |French        |1600-1649          |Folio     |    21|         50.0|
|The Hague  |French        |1600-1649          |Duodecimo |     5|         11.9|
|The Hague  |Latin         |1600-1649          |Quarto    |    74|         51.4|
|The Hague  |Latin         |1600-1649          |Octavo    |    49|         34.0|
|The Hague  |Latin         |1600-1649          |Folio     |     4|          2.8|
|The Hague  |Latin         |1600-1649          |Duodecimo |    17|         11.8|
|The Hague  |Undetermined  |1600-1649          |Quarto    |   261|         96.0|
|The Hague  |Undetermined  |1600-1649          |Octavo    |     2|          0.7|
|The Hague  |Undetermined  |1600-1649          |Folio     |     8|          2.9|
|The Hague  |Undetermined  |1600-1649          |Duodecimo |     1|          0.4|
|Turku      |Finnish       |1600-1649          |Quarto    |    10|         58.8|
|Turku      |Finnish       |1600-1649          |Octavo    |     7|         41.2|
|Turku      |Latin         |1600-1649          |Quarto    |   232|         84.1|
|Turku      |Latin         |1600-1649          |Octavo    |    32|         11.6|
|Turku      |Latin         |1600-1649          |Folio     |     7|          2.5|
|Turku      |Latin         |1600-1649          |Duodecimo |     5|          1.8|
|Turku      |Swedish       |1600-1649          |Quarto    |    21|         60.0|
|Turku      |Swedish       |1600-1649          |Octavo    |    12|         34.3|
|Turku      |Swedish       |1600-1649          |Folio     |     2|          5.7|
|Utrecht    |French        |1600-1649          |Quarto    |     1|         50.0|
|Utrecht    |French        |1600-1649          |Duodecimo |     1|         50.0|
|Utrecht    |Latin         |1600-1649          |Quarto    |    47|         60.3|
|Utrecht    |Latin         |1600-1649          |Octavo    |    17|         21.8|
|Utrecht    |Latin         |1600-1649          |Folio     |     5|          6.4|
|Utrecht    |Latin         |1600-1649          |Duodecimo |     9|         11.5|
|Utrecht    |Undetermined  |1600-1649          |Quarto    |    55|         91.7|
|Utrecht    |Undetermined  |1600-1649          |Octavo    |     2|          3.3|
|Utrecht    |Undetermined  |1600-1649          |Folio     |     1|          1.7|
|Utrecht    |Undetermined  |1600-1649          |Duodecimo |     2|          3.3|
|Venice     |Italian       |1600-1649          |Quarto    |   501|         29.9|
|Venice     |Italian       |1600-1649          |Octavo    |   886|         52.9|
|Venice     |Italian       |1600-1649          |Folio     |   116|          6.9|
|Venice     |Italian       |1600-1649          |Duodecimo |   171|         10.2|
|Venice     |Latin         |1600-1649          |Quarto    |   273|         28.3|
|Venice     |Latin         |1600-1649          |Octavo    |   159|         16.5|
|Venice     |Latin         |1600-1649          |Folio     |   520|         53.9|
|Venice     |Latin         |1600-1649          |Duodecimo |    13|          1.3|
|Venice     |Undetermined  |1600-1649          |Quarto    |    37|         42.5|
|Venice     |Undetermined  |1600-1649          |Octavo    |     3|          3.4|
|Venice     |Undetermined  |1600-1649          |Folio     |    35|         40.2|
|Venice     |Undetermined  |1600-1649          |Duodecimo |    12|         13.8|
|Vienna     |German        |1600-1649          |Quarto    |    43|         58.1|
|Vienna     |German        |1600-1649          |Octavo    |    14|         18.9|
|Vienna     |German        |1600-1649          |Folio     |    14|         18.9|
|Vienna     |German        |1600-1649          |Duodecimo |     3|          4.1|
|Vienna     |Latin         |1600-1649          |Quarto    |    45|         45.9|
|Vienna     |Latin         |1600-1649          |Octavo    |    29|         29.6|
|Vienna     |Latin         |1600-1649          |Folio     |    17|         17.3|
|Vienna     |Latin         |1600-1649          |Duodecimo |     7|          7.1|
|Vienna     |Undetermined  |1600-1649          |Quarto    |     2|         66.7|
|Vienna     |Undetermined  |1600-1649          |Folio     |     1|         33.3|
|Wittenberg |German        |1600-1649          |Quarto    |   741|         78.6|
|Wittenberg |German        |1600-1649          |Octavo    |   144|         15.3|
|Wittenberg |German        |1600-1649          |Folio     |    22|          2.3|
|Wittenberg |German        |1600-1649          |Duodecimo |    36|          3.8|
|Wittenberg |Latin         |1600-1649          |Quarto    |  3535|         76.6|
|Wittenberg |Latin         |1600-1649          |Octavo    |   878|         19.0|
|Wittenberg |Latin         |1600-1649          |Folio     |   103|          2.2|
|Wittenberg |Latin         |1600-1649          |Duodecimo |   100|          2.2|
|Wittenberg |Latin;German  |1600-1649          |Quarto    |    15|         50.0|
|Wittenberg |Latin;German  |1600-1649          |Octavo    |    11|         36.7|
|Wittenberg |Latin;German  |1600-1649          |Folio     |     3|         10.0|
|Wittenberg |Latin;German  |1600-1649          |Duodecimo |     1|          3.3|
|Amsterdam  |Dutch         |1650-1699          |Quarto    |   628|         43.5|
|Amsterdam  |Dutch         |1650-1699          |Octavo    |   529|         36.7|
|Amsterdam  |Dutch         |1650-1699          |Folio     |   156|         10.8|
|Amsterdam  |Dutch         |1650-1699          |Duodecimo |   130|          9.0|
|Amsterdam  |French        |1650-1699          |Quarto    |    74|          4.5|
|Amsterdam  |French        |1650-1699          |Octavo    |   546|         33.5|
|Amsterdam  |French        |1650-1699          |Folio     |    72|          4.4|
|Amsterdam  |French        |1650-1699          |Duodecimo |   938|         57.5|
|Amsterdam  |Latin         |1650-1699          |Quarto    |   716|         25.0|
|Amsterdam  |Latin         |1650-1699          |Octavo    |   950|         33.2|
|Amsterdam  |Latin         |1650-1699          |Folio     |   450|         15.7|
|Amsterdam  |Latin         |1650-1699          |Duodecimo |   746|         26.1|
|Berlin     |French        |1650-1699          |Quarto    |     8|         16.3|
|Berlin     |French        |1650-1699          |Octavo    |    23|         46.9|
|Berlin     |French        |1650-1699          |Folio     |     3|          6.1|
|Berlin     |French        |1650-1699          |Duodecimo |    15|         30.6|
|Berlin     |German        |1650-1699          |Quarto    |   622|         64.9|
|Berlin     |German        |1650-1699          |Octavo    |   121|         12.6|
|Berlin     |German        |1650-1699          |Folio     |   141|         14.7|
|Berlin     |German        |1650-1699          |Duodecimo |    74|          7.7|
|Berlin     |Latin         |1650-1699          |Quarto    |   154|         39.9|
|Berlin     |Latin         |1650-1699          |Octavo    |   118|         30.6|
|Berlin     |Latin         |1650-1699          |Folio     |    84|         21.8|
|Berlin     |Latin         |1650-1699          |Duodecimo |    30|          7.8|
|Brussels   |French        |1650-1699          |Quarto    |    12|          5.3|
|Brussels   |French        |1650-1699          |Octavo    |    63|         28.0|
|Brussels   |French        |1650-1699          |Folio     |     8|          3.6|
|Brussels   |French        |1650-1699          |Duodecimo |   142|         63.1|
|Brussels   |Latin         |1650-1699          |Quarto    |    17|         21.0|
|Brussels   |Latin         |1650-1699          |Octavo    |    29|         35.8|
|Brussels   |Latin         |1650-1699          |Folio     |    19|         23.5|
|Brussels   |Latin         |1650-1699          |Duodecimo |    16|         19.8|
|Brussels   |Undetermined  |1650-1699          |Quarto    |   763|         50.5|
|Brussels   |Undetermined  |1650-1699          |Octavo    |   171|         11.3|
|Brussels   |Undetermined  |1650-1699          |Folio     |   252|         16.7|
|Brussels   |Undetermined  |1650-1699          |Duodecimo |   324|         21.5|
|Edinburgh  |English       |1650-1699          |Quarto    |   166|         23.4|
|Edinburgh  |English       |1650-1699          |Octavo    |    87|         12.3|
|Edinburgh  |English       |1650-1699          |Folio     |   418|         59.0|
|Edinburgh  |English       |1650-1699          |Duodecimo |    38|          5.4|
|Edinburgh  |Latin         |1650-1699          |Quarto    |    11|         32.4|
|Edinburgh  |Latin         |1650-1699          |Octavo    |     9|         26.5|
|Edinburgh  |Latin         |1650-1699          |Folio     |    12|         35.3|
|Edinburgh  |Latin         |1650-1699          |Duodecimo |     2|          5.9|
|Edinburgh  |Undetermined  |1650-1699          |Quarto    |     9|         20.5|
|Edinburgh  |Undetermined  |1650-1699          |Octavo    |     3|          6.8|
|Edinburgh  |Undetermined  |1650-1699          |Folio     |    31|         70.5|
|Edinburgh  |Undetermined  |1650-1699          |Duodecimo |     1|          2.3|
|Frankfurt  |German        |1650-1699          |Quarto    |  1289|         33.7|
|Frankfurt  |German        |1650-1699          |Octavo    |  1152|         30.1|
|Frankfurt  |German        |1650-1699          |Folio     |   821|         21.5|
|Frankfurt  |German        |1650-1699          |Duodecimo |   560|         14.7|
|Frankfurt  |Latin         |1650-1699          |Quarto    |  2765|         53.4|
|Frankfurt  |Latin         |1650-1699          |Octavo    |  1057|         20.4|
|Frankfurt  |Latin         |1650-1699          |Folio     |   922|         17.8|
|Frankfurt  |Latin         |1650-1699          |Duodecimo |   432|          8.3|
|Frankfurt  |Undetermined  |1650-1699          |Quarto    |    85|         70.8|
|Frankfurt  |Undetermined  |1650-1699          |Octavo    |    11|          9.2|
|Frankfurt  |Undetermined  |1650-1699          |Folio     |    17|         14.2|
|Frankfurt  |Undetermined  |1650-1699          |Duodecimo |     7|          5.8|
|Göttingen  |German        |1650-1699          |Quarto    |    23|         54.8|
|Göttingen  |German        |1650-1699          |Octavo    |    11|         26.2|
|Göttingen  |German        |1650-1699          |Folio     |     2|          4.8|
|Göttingen  |German        |1650-1699          |Duodecimo |     6|         14.3|
|Göttingen  |Latin         |1650-1699          |Quarto    |    17|         45.9|
|Göttingen  |Latin         |1650-1699          |Octavo    |    14|         37.8|
|Göttingen  |Latin         |1650-1699          |Folio     |     2|          5.4|
|Göttingen  |Latin         |1650-1699          |Duodecimo |     4|         10.8|
|Halle      |German        |1650-1699          |Quarto    |   494|         48.4|
|Halle      |German        |1650-1699          |Octavo    |   195|         19.1|
|Halle      |German        |1650-1699          |Folio     |   214|         21.0|
|Halle      |German        |1650-1699          |Duodecimo |   118|         11.6|
|Halle      |Latin         |1650-1699          |Quarto    |   958|         82.5|
|Halle      |Latin         |1650-1699          |Octavo    |   103|          8.9|
|Halle      |Latin         |1650-1699          |Folio     |    82|          7.1|
|Halle      |Latin         |1650-1699          |Duodecimo |    18|          1.6|
|Halle      |Latin;German  |1650-1699          |Quarto    |    37|         75.5|
|Halle      |Latin;German  |1650-1699          |Octavo    |     6|         12.2|
|Halle      |Latin;German  |1650-1699          |Folio     |     4|          8.2|
|Halle      |Latin;German  |1650-1699          |Duodecimo |     2|          4.1|
|Hamburg    |French        |1650-1699          |Quarto    |     4|         15.4|
|Hamburg    |French        |1650-1699          |Octavo    |    15|         57.7|
|Hamburg    |French        |1650-1699          |Duodecimo |     7|         26.9|
|Hamburg    |German        |1650-1699          |Quarto    |  1046|         51.2|
|Hamburg    |German        |1650-1699          |Octavo    |   644|         31.6|
|Hamburg    |German        |1650-1699          |Folio     |    76|          3.7|
|Hamburg    |German        |1650-1699          |Duodecimo |   275|         13.5|
|Hamburg    |Latin         |1650-1699          |Quarto    |   298|         43.1|
|Hamburg    |Latin         |1650-1699          |Octavo    |   259|         37.5|
|Hamburg    |Latin         |1650-1699          |Folio     |    68|          9.8|
|Hamburg    |Latin         |1650-1699          |Duodecimo |    66|          9.6|
|Jena       |German        |1650-1699          |Quarto    |   565|         45.7|
|Jena       |German        |1650-1699          |Octavo    |   241|         19.5|
|Jena       |German        |1650-1699          |Folio     |   355|         28.7|
|Jena       |German        |1650-1699          |Duodecimo |    76|          6.1|
|Jena       |Latin         |1650-1699          |Quarto    |  2533|         75.1|
|Jena       |Latin         |1650-1699          |Octavo    |   630|         18.7|
|Jena       |Latin         |1650-1699          |Folio     |   122|          3.6|
|Jena       |Latin         |1650-1699          |Duodecimo |    90|          2.7|
|Jena       |Latin;German  |1650-1699          |Quarto    |    89|         78.8|
|Jena       |Latin;German  |1650-1699          |Octavo    |    21|         18.6|
|Jena       |Latin;German  |1650-1699          |Folio     |     3|          2.7|
|Leiden     |French        |1650-1699          |Quarto    |    10|          4.5|
|Leiden     |French        |1650-1699          |Octavo    |    62|         27.7|
|Leiden     |French        |1650-1699          |Folio     |     8|          3.6|
|Leiden     |French        |1650-1699          |Duodecimo |   144|         64.3|
|Leiden     |Latin         |1650-1699          |Quarto    |   499|         41.1|
|Leiden     |Latin         |1650-1699          |Octavo    |   405|         33.3|
|Leiden     |Latin         |1650-1699          |Folio     |    80|          6.6|
|Leiden     |Latin         |1650-1699          |Duodecimo |   231|         19.0|
|Leiden     |Undetermined  |1650-1699          |Quarto    |    39|         39.0|
|Leiden     |Undetermined  |1650-1699          |Octavo    |    19|         19.0|
|Leiden     |Undetermined  |1650-1699          |Folio     |     7|          7.0|
|Leiden     |Undetermined  |1650-1699          |Duodecimo |    35|         35.0|
|Leipzig    |German        |1650-1699          |Quarto    |  1434|         42.1|
|Leipzig    |German        |1650-1699          |Octavo    |  1006|         29.5|
|Leipzig    |German        |1650-1699          |Folio     |   567|         16.7|
|Leipzig    |German        |1650-1699          |Duodecimo |   398|         11.7|
|Leipzig    |Latin         |1650-1699          |Quarto    |  4074|         73.2|
|Leipzig    |Latin         |1650-1699          |Octavo    |   828|         14.9|
|Leipzig    |Latin         |1650-1699          |Folio     |   386|          6.9|
|Leipzig    |Latin         |1650-1699          |Duodecimo |   279|          5.0|
|Leipzig    |Undetermined  |1650-1699          |Quarto    |    36|         75.0|
|Leipzig    |Undetermined  |1650-1699          |Octavo    |     9|         18.8|
|Leipzig    |Undetermined  |1650-1699          |Folio     |     2|          4.2|
|Leipzig    |Undetermined  |1650-1699          |Duodecimo |     1|          2.1|
|London     |English       |1650-1699          |Quarto    |  3364|         28.1|
|London     |English       |1650-1699          |Octavo    |  3026|         25.3|
|London     |English       |1650-1699          |Folio     |  4918|         41.1|
|London     |English       |1650-1699          |Duodecimo |   660|          5.5|
|London     |Latin         |1650-1699          |Quarto    |   257|         24.1|
|London     |Latin         |1650-1699          |Octavo    |   440|         41.3|
|London     |Latin         |1650-1699          |Folio     |   228|         21.4|
|London     |Latin         |1650-1699          |Duodecimo |   141|         13.2|
|London     |Undetermined  |1650-1699          |Quarto    |   806|         35.5|
|London     |Undetermined  |1650-1699          |Octavo    |   622|         27.4|
|London     |Undetermined  |1650-1699          |Folio     |   741|         32.6|
|London     |Undetermined  |1650-1699          |Duodecimo |   102|          4.5|
|Madrid     |Latin         |1650-1699          |Octavo    |     2|         15.4|
|Madrid     |Latin         |1650-1699          |Folio     |    11|         84.6|
|Madrid     |Panjabi       |1650-1699          |Quarto    |    27|         42.9|
|Madrid     |Panjabi       |1650-1699          |Octavo    |    11|         17.5|
|Madrid     |Panjabi       |1650-1699          |Folio     |    25|         39.7|
|Madrid     |Spanish       |1650-1699          |Quarto    |   107|         13.3|
|Madrid     |Spanish       |1650-1699          |Octavo    |    98|         12.2|
|Madrid     |Spanish       |1650-1699          |Folio     |   595|         74.0|
|Madrid     |Spanish       |1650-1699          |Duodecimo |     4|          0.5|
|Nürnberg   |German        |1650-1699          |Quarto    |   719|         29.5|
|Nürnberg   |German        |1650-1699          |Octavo    |   860|         35.3|
|Nürnberg   |German        |1650-1699          |Folio     |   305|         12.5|
|Nürnberg   |German        |1650-1699          |Duodecimo |   552|         22.7|
|Nürnberg   |Latin         |1650-1699          |Quarto    |   290|         36.0|
|Nürnberg   |Latin         |1650-1699          |Octavo    |   248|         30.8|
|Nürnberg   |Latin         |1650-1699          |Folio     |   151|         18.8|
|Nürnberg   |Latin         |1650-1699          |Duodecimo |   116|         14.4|
|Nürnberg   |Undetermined  |1650-1699          |Quarto    |    10|         31.2|
|Nürnberg   |Undetermined  |1650-1699          |Octavo    |     5|         15.6|
|Nürnberg   |Undetermined  |1650-1699          |Folio     |     5|         15.6|
|Nürnberg   |Undetermined  |1650-1699          |Duodecimo |    12|         37.5|
|Paris      |French        |1650-1699          |Quarto    |  2022|         27.1|
|Paris      |French        |1650-1699          |Octavo    |  2661|         35.6|
|Paris      |French        |1650-1699          |Folio     |   685|          9.2|
|Paris      |French        |1650-1699          |Duodecimo |  2104|         28.2|
|Paris      |Latin         |1650-1699          |Quarto    |   503|         29.4|
|Paris      |Latin         |1650-1699          |Octavo    |   435|         25.4|
|Paris      |Latin         |1650-1699          |Folio     |   666|         38.9|
|Paris      |Latin         |1650-1699          |Duodecimo |   108|          6.3|
|Paris      |Undetermined  |1650-1699          |Quarto    |   255|         32.6|
|Paris      |Undetermined  |1650-1699          |Octavo    |   152|         19.4|
|Paris      |Undetermined  |1650-1699          |Folio     |   156|         19.9|
|Paris      |Undetermined  |1650-1699          |Duodecimo |   220|         28.1|
|Prague     |Czech         |1650-1699          |Quarto    |     2|         66.7|
|Prague     |Czech         |1650-1699          |Octavo    |     1|         33.3|
|Prague     |German        |1650-1699          |Quarto    |    40|         42.1|
|Prague     |German        |1650-1699          |Octavo    |    30|         31.6|
|Prague     |German        |1650-1699          |Folio     |    15|         15.8|
|Prague     |German        |1650-1699          |Duodecimo |    10|         10.5|
|Prague     |Latin         |1650-1699          |Quarto    |    59|         22.7|
|Prague     |Latin         |1650-1699          |Octavo    |    42|         16.2|
|Prague     |Latin         |1650-1699          |Folio     |   135|         51.9|
|Prague     |Latin         |1650-1699          |Duodecimo |    24|          9.2|
|Rome       |Italian       |1650-1699          |Quarto    |   259|         28.8|
|Rome       |Italian       |1650-1699          |Octavo    |   154|         17.1|
|Rome       |Italian       |1650-1699          |Folio     |   454|         50.5|
|Rome       |Italian       |1650-1699          |Duodecimo |    32|          3.6|
|Rome       |Italian;Latin |1650-1699          |Quarto    |    17|          3.3|
|Rome       |Italian;Latin |1650-1699          |Octavo    |     5|          1.0|
|Rome       |Italian;Latin |1650-1699          |Folio     |   486|         95.7|
|Rome       |Latin         |1650-1699          |Quarto    |   352|         19.6|
|Rome       |Latin         |1650-1699          |Octavo    |   137|          7.6|
|Rome       |Latin         |1650-1699          |Folio     |  1286|         71.6|
|Rome       |Latin         |1650-1699          |Duodecimo |    22|          1.2|
|Stockholm  |Finnish       |1650-1699          |Quarto    |    10|         20.4|
|Stockholm  |Finnish       |1650-1699          |Octavo    |     4|          8.2|
|Stockholm  |Finnish       |1650-1699          |Folio     |     1|          2.0|
|Stockholm  |Finnish       |1650-1699          |Duodecimo |    34|         69.4|
|Stockholm  |Latin         |1650-1699          |Quarto    |   111|         25.6|
|Stockholm  |Latin         |1650-1699          |Octavo    |   226|         52.2|
|Stockholm  |Latin         |1650-1699          |Folio     |    70|         16.2|
|Stockholm  |Latin         |1650-1699          |Duodecimo |    26|          6.0|
|Stockholm  |Swedish       |1650-1699          |Quarto    |  1077|         55.2|
|Stockholm  |Swedish       |1650-1699          |Octavo    |   290|         14.9|
|Stockholm  |Swedish       |1650-1699          |Folio     |   406|         20.8|
|Stockholm  |Swedish       |1650-1699          |Duodecimo |   178|          9.1|
|Strasbourg |French        |1650-1699          |Quarto    |     6|         15.4|
|Strasbourg |French        |1650-1699          |Octavo    |    16|         41.0|
|Strasbourg |French        |1650-1699          |Folio     |     3|          7.7|
|Strasbourg |French        |1650-1699          |Duodecimo |    14|         35.9|
|Strasbourg |German        |1650-1699          |Quarto    |   164|         41.5|
|Strasbourg |German        |1650-1699          |Octavo    |    91|         23.0|
|Strasbourg |German        |1650-1699          |Folio     |   116|         29.4|
|Strasbourg |German        |1650-1699          |Duodecimo |    24|          6.1|
|Strasbourg |Latin         |1650-1699          |Quarto    |  1610|         73.1|
|Strasbourg |Latin         |1650-1699          |Octavo    |   340|         15.4|
|Strasbourg |Latin         |1650-1699          |Folio     |   203|          9.2|
|Strasbourg |Latin         |1650-1699          |Duodecimo |    49|          2.2|
|The Hague  |French        |1650-1699          |Quarto    |    33|          6.4|
|The Hague  |French        |1650-1699          |Octavo    |   105|         20.4|
|The Hague  |French        |1650-1699          |Folio     |    20|          3.9|
|The Hague  |French        |1650-1699          |Duodecimo |   357|         69.3|
|The Hague  |Latin         |1650-1699          |Quarto    |   148|         41.9|
|The Hague  |Latin         |1650-1699          |Octavo    |   124|         35.1|
|The Hague  |Latin         |1650-1699          |Folio     |     8|          2.3|
|The Hague  |Latin         |1650-1699          |Duodecimo |    73|         20.7|
|The Hague  |Undetermined  |1650-1699          |Quarto    |    25|         30.1|
|The Hague  |Undetermined  |1650-1699          |Octavo    |    11|         13.3|
|The Hague  |Undetermined  |1650-1699          |Folio     |    22|         26.5|
|The Hague  |Undetermined  |1650-1699          |Duodecimo |    25|         30.1|
|Turku      |Finnish       |1650-1699          |Quarto    |   165|         74.3|
|Turku      |Finnish       |1650-1699          |Octavo    |    42|         18.9|
|Turku      |Finnish       |1650-1699          |Folio     |     2|          0.9|
|Turku      |Finnish       |1650-1699          |Duodecimo |    13|          5.9|
|Turku      |Latin         |1650-1699          |Quarto    |  1305|         57.2|
|Turku      |Latin         |1650-1699          |Octavo    |   786|         34.4|
|Turku      |Latin         |1650-1699          |Folio     |   155|          6.8|
|Turku      |Latin         |1650-1699          |Duodecimo |    37|          1.6|
|Turku      |Swedish       |1650-1699          |Quarto    |   458|         66.2|
|Turku      |Swedish       |1650-1699          |Octavo    |    33|          4.8|
|Turku      |Swedish       |1650-1699          |Folio     |   181|         26.2|
|Turku      |Swedish       |1650-1699          |Duodecimo |    20|          2.9|
|Utrecht    |French        |1650-1699          |Octavo    |    22|         31.4|
|Utrecht    |French        |1650-1699          |Duodecimo |    48|         68.6|
|Utrecht    |Latin         |1650-1699          |Quarto    |   220|         62.1|
|Utrecht    |Latin         |1650-1699          |Octavo    |    91|         25.7|
|Utrecht    |Latin         |1650-1699          |Folio     |    20|          5.6|
|Utrecht    |Latin         |1650-1699          |Duodecimo |    23|          6.5|
|Utrecht    |Undetermined  |1650-1699          |Quarto    |    11|         44.0|
|Utrecht    |Undetermined  |1650-1699          |Octavo    |     8|         32.0|
|Utrecht    |Undetermined  |1650-1699          |Folio     |     1|          4.0|
|Utrecht    |Undetermined  |1650-1699          |Duodecimo |     5|         20.0|
|Venice     |Italian       |1650-1699          |Quarto    |   288|         25.8|
|Venice     |Italian       |1650-1699          |Octavo    |   426|         38.2|
|Venice     |Italian       |1650-1699          |Folio     |   104|          9.3|
|Venice     |Italian       |1650-1699          |Duodecimo |   298|         26.7|
|Venice     |Latin         |1650-1699          |Quarto    |   101|         20.9|
|Venice     |Latin         |1650-1699          |Octavo    |    66|         13.7|
|Venice     |Latin         |1650-1699          |Folio     |   289|         59.8|
|Venice     |Latin         |1650-1699          |Duodecimo |    27|          5.6|
|Venice     |Undetermined  |1650-1699          |Quarto    |    10|         45.5|
|Venice     |Undetermined  |1650-1699          |Octavo    |     3|         13.6|
|Venice     |Undetermined  |1650-1699          |Folio     |     8|         36.4|
|Venice     |Undetermined  |1650-1699          |Duodecimo |     1|          4.5|
|Vienna     |German        |1650-1699          |Quarto    |    60|         38.7|
|Vienna     |German        |1650-1699          |Octavo    |    47|         30.3|
|Vienna     |German        |1650-1699          |Folio     |    37|         23.9|
|Vienna     |German        |1650-1699          |Duodecimo |    11|          7.1|
|Vienna     |Latin         |1650-1699          |Quarto    |    71|         29.2|
|Vienna     |Latin         |1650-1699          |Octavo    |    57|         23.5|
|Vienna     |Latin         |1650-1699          |Folio     |    75|         30.9|
|Vienna     |Latin         |1650-1699          |Duodecimo |    40|         16.5|
|Vienna     |Undetermined  |1650-1699          |Quarto    |     2|         25.0|
|Vienna     |Undetermined  |1650-1699          |Octavo    |     1|         12.5|
|Vienna     |Undetermined  |1650-1699          |Folio     |     3|         37.5|
|Vienna     |Undetermined  |1650-1699          |Duodecimo |     2|         25.0|
|Wittenberg |German        |1650-1699          |Quarto    |   382|         47.6|
|Wittenberg |German        |1650-1699          |Octavo    |   114|         14.2|
|Wittenberg |German        |1650-1699          |Folio     |   271|         33.7|
|Wittenberg |German        |1650-1699          |Duodecimo |    36|          4.5|
|Wittenberg |Latin         |1650-1699          |Quarto    |  5956|         88.0|
|Wittenberg |Latin         |1650-1699          |Octavo    |   413|          6.1|
|Wittenberg |Latin         |1650-1699          |Folio     |   323|          4.8|
|Wittenberg |Latin         |1650-1699          |Duodecimo |    79|          1.2|
|Wittenberg |Latin;German  |1650-1699          |Quarto    |    56|         45.5|
|Wittenberg |Latin;German  |1650-1699          |Octavo    |     4|          3.3|
|Wittenberg |Latin;German  |1650-1699          |Folio     |    62|         50.4|
|Wittenberg |Latin;German  |1650-1699          |Duodecimo |     1|          0.8|
|Amsterdam  |Dutch         |1700-1749          |Quarto    |   159|         25.9|
|Amsterdam  |Dutch         |1700-1749          |Octavo    |   323|         52.6|
|Amsterdam  |Dutch         |1700-1749          |Folio     |   108|         17.6|
|Amsterdam  |Dutch         |1700-1749          |Duodecimo |    24|          3.9|
|Amsterdam  |French        |1700-1749          |Quarto    |   443|         12.1|
|Amsterdam  |French        |1700-1749          |Octavo    |  1438|         39.3|
|Amsterdam  |French        |1700-1749          |Folio     |   349|          9.5|
|Amsterdam  |French        |1700-1749          |Duodecimo |  1428|         39.0|
|Amsterdam  |Latin         |1700-1749          |Quarto    |   456|         37.8|
|Amsterdam  |Latin         |1700-1749          |Octavo    |   481|         39.9|
|Amsterdam  |Latin         |1700-1749          |Folio     |   208|         17.2|
|Amsterdam  |Latin         |1700-1749          |Duodecimo |    62|          5.1|
|Berlin     |French        |1700-1749          |Quarto    |    43|         32.8|
|Berlin     |French        |1700-1749          |Octavo    |    68|         51.9|
|Berlin     |French        |1700-1749          |Folio     |     7|          5.3|
|Berlin     |French        |1700-1749          |Duodecimo |    13|          9.9|
|Berlin     |German        |1700-1749          |Quarto    |   674|         33.3|
|Berlin     |German        |1700-1749          |Octavo    |   554|         27.3|
|Berlin     |German        |1700-1749          |Folio     |   760|         37.5|
|Berlin     |German        |1700-1749          |Duodecimo |    38|          1.9|
|Berlin     |Latin         |1700-1749          |Quarto    |   127|         39.4|
|Berlin     |Latin         |1700-1749          |Octavo    |   130|         40.4|
|Berlin     |Latin         |1700-1749          |Folio     |    55|         17.1|
|Berlin     |Latin         |1700-1749          |Duodecimo |    10|          3.1|
|Brussels   |French        |1700-1749          |Quarto    |    25|          7.2|
|Brussels   |French        |1700-1749          |Octavo    |   139|         40.3|
|Brussels   |French        |1700-1749          |Folio     |    17|          4.9|
|Brussels   |French        |1700-1749          |Duodecimo |   164|         47.5|
|Brussels   |Latin         |1700-1749          |Quarto    |     9|         16.1|
|Brussels   |Latin         |1700-1749          |Octavo    |    18|         32.1|
|Brussels   |Latin         |1700-1749          |Folio     |    25|         44.6|
|Brussels   |Latin         |1700-1749          |Duodecimo |     4|          7.1|
|Brussels   |Undetermined  |1700-1749          |Quarto    |   806|         44.7|
|Brussels   |Undetermined  |1700-1749          |Octavo    |   317|         17.6|
|Brussels   |Undetermined  |1700-1749          |Folio     |   431|         23.9|
|Brussels   |Undetermined  |1700-1749          |Duodecimo |   248|         13.8|
|Edinburgh  |English       |1700-1749          |Quarto    |   360|         26.0|
|Edinburgh  |English       |1700-1749          |Octavo    |   282|         20.3|
|Edinburgh  |English       |1700-1749          |Folio     |   709|         51.2|
|Edinburgh  |English       |1700-1749          |Duodecimo |    35|          2.5|
|Edinburgh  |Latin         |1700-1749          |Quarto    |    40|         49.4|
|Edinburgh  |Latin         |1700-1749          |Octavo    |    20|         24.7|
|Edinburgh  |Latin         |1700-1749          |Folio     |    12|         14.8|
|Edinburgh  |Latin         |1700-1749          |Duodecimo |     9|         11.1|
|Edinburgh  |Undetermined  |1700-1749          |Quarto    |    45|         39.8|
|Edinburgh  |Undetermined  |1700-1749          |Octavo    |    30|         26.5|
|Edinburgh  |Undetermined  |1700-1749          |Folio     |    36|         31.9|
|Edinburgh  |Undetermined  |1700-1749          |Duodecimo |     2|          1.8|
|Frankfurt  |German        |1700-1749          |Quarto    |   887|         25.7|
|Frankfurt  |German        |1700-1749          |Octavo    |  1869|         54.2|
|Frankfurt  |German        |1700-1749          |Folio     |   566|         16.4|
|Frankfurt  |German        |1700-1749          |Duodecimo |   128|          3.7|
|Frankfurt  |Latin         |1700-1749          |Quarto    |  1012|         51.3|
|Frankfurt  |Latin         |1700-1749          |Octavo    |   555|         28.2|
|Frankfurt  |Latin         |1700-1749          |Folio     |   356|         18.1|
|Frankfurt  |Latin         |1700-1749          |Duodecimo |    48|          2.4|
|Frankfurt  |Undetermined  |1700-1749          |Quarto    |   111|         45.1|
|Frankfurt  |Undetermined  |1700-1749          |Octavo    |    95|         38.6|
|Frankfurt  |Undetermined  |1700-1749          |Folio     |    29|         11.8|
|Frankfurt  |Undetermined  |1700-1749          |Duodecimo |    11|          4.5|
|Göttingen  |German        |1700-1749          |Quarto    |   103|         34.6|
|Göttingen  |German        |1700-1749          |Octavo    |    81|         27.2|
|Göttingen  |German        |1700-1749          |Folio     |   112|         37.6|
|Göttingen  |German        |1700-1749          |Duodecimo |     2|          0.7|
|Göttingen  |Latin         |1700-1749          |Quarto    |   611|         68.9|
|Göttingen  |Latin         |1700-1749          |Octavo    |   171|         19.3|
|Göttingen  |Latin         |1700-1749          |Folio     |   102|         11.5|
|Göttingen  |Latin         |1700-1749          |Duodecimo |     3|          0.3|
|Göttingen  |Undetermined  |1700-1749          |Quarto    |    21|         75.0|
|Göttingen  |Undetermined  |1700-1749          |Octavo    |     4|         14.3|
|Göttingen  |Undetermined  |1700-1749          |Folio     |     3|         10.7|
|Halle      |German        |1700-1749          |Quarto    |   710|         19.3|
|Halle      |German        |1700-1749          |Octavo    |  1535|         41.8|
|Halle      |German        |1700-1749          |Folio     |   996|         27.1|
|Halle      |German        |1700-1749          |Duodecimo |   429|         11.7|
|Halle      |Latin         |1700-1749          |Quarto    |  3609|         81.0|
|Halle      |Latin         |1700-1749          |Octavo    |   606|         13.6|
|Halle      |Latin         |1700-1749          |Folio     |   231|          5.2|
|Halle      |Latin         |1700-1749          |Duodecimo |    12|          0.3|
|Halle      |Latin;German  |1700-1749          |Quarto    |   302|         81.2|
|Halle      |Latin;German  |1700-1749          |Octavo    |    47|         12.6|
|Halle      |Latin;German  |1700-1749          |Folio     |    21|          5.6|
|Halle      |Latin;German  |1700-1749          |Duodecimo |     2|          0.5|
|Hamburg    |French        |1700-1749          |Quarto    |     3|          7.7|
|Hamburg    |French        |1700-1749          |Octavo    |    19|         48.7|
|Hamburg    |French        |1700-1749          |Folio     |     2|          5.1|
|Hamburg    |French        |1700-1749          |Duodecimo |    15|         38.5|
|Hamburg    |German        |1700-1749          |Quarto    |   852|         36.9|
|Hamburg    |German        |1700-1749          |Octavo    |  1050|         45.4|
|Hamburg    |German        |1700-1749          |Folio     |   190|          8.2|
|Hamburg    |German        |1700-1749          |Duodecimo |   219|          9.5|
|Hamburg    |Latin         |1700-1749          |Quarto    |   179|         35.7|
|Hamburg    |Latin         |1700-1749          |Octavo    |   238|         47.5|
|Hamburg    |Latin         |1700-1749          |Folio     |    81|         16.2|
|Hamburg    |Latin         |1700-1749          |Duodecimo |     3|          0.6|
|Jena       |German        |1700-1749          |Quarto    |   289|         22.9|
|Jena       |German        |1700-1749          |Octavo    |   601|         47.7|
|Jena       |German        |1700-1749          |Folio     |   339|         26.9|
|Jena       |German        |1700-1749          |Duodecimo |    31|          2.5|
|Jena       |Latin         |1700-1749          |Quarto    |  1679|         79.1|
|Jena       |Latin         |1700-1749          |Octavo    |   353|         16.6|
|Jena       |Latin         |1700-1749          |Folio     |    69|          3.3|
|Jena       |Latin         |1700-1749          |Duodecimo |    22|          1.0|
|Jena       |Latin;German  |1700-1749          |Quarto    |   114|         83.2|
|Jena       |Latin;German  |1700-1749          |Octavo    |    16|         11.7|
|Jena       |Latin;German  |1700-1749          |Folio     |     6|          4.4|
|Jena       |Latin;German  |1700-1749          |Duodecimo |     1|          0.7|
|Leiden     |French        |1700-1749          |Quarto    |   102|         27.4|
|Leiden     |French        |1700-1749          |Octavo    |    80|         21.5|
|Leiden     |French        |1700-1749          |Folio     |   124|         33.3|
|Leiden     |French        |1700-1749          |Duodecimo |    66|         17.7|
|Leiden     |Latin         |1700-1749          |Quarto    |   812|         55.1|
|Leiden     |Latin         |1700-1749          |Octavo    |   454|         30.8|
|Leiden     |Latin         |1700-1749          |Folio     |   185|         12.6|
|Leiden     |Latin         |1700-1749          |Duodecimo |    23|          1.6|
|Leiden     |Undetermined  |1700-1749          |Quarto    |   273|         81.5|
|Leiden     |Undetermined  |1700-1749          |Octavo    |    22|          6.6|
|Leiden     |Undetermined  |1700-1749          |Folio     |    11|          3.3|
|Leiden     |Undetermined  |1700-1749          |Duodecimo |    29|          8.7|
|Leipzig    |German        |1700-1749          |Quarto    |  1469|         29.6|
|Leipzig    |German        |1700-1749          |Octavo    |  2463|         49.6|
|Leipzig    |German        |1700-1749          |Folio     |   788|         15.9|
|Leipzig    |German        |1700-1749          |Duodecimo |   245|          4.9|
|Leipzig    |Latin         |1700-1749          |Quarto    |  3408|         68.6|
|Leipzig    |Latin         |1700-1749          |Octavo    |  1160|         23.3|
|Leipzig    |Latin         |1700-1749          |Folio     |   300|          6.0|
|Leipzig    |Latin         |1700-1749          |Duodecimo |   101|          2.0|
|Leipzig    |Undetermined  |1700-1749          |Quarto    |   356|         73.9|
|Leipzig    |Undetermined  |1700-1749          |Octavo    |    98|         20.3|
|Leipzig    |Undetermined  |1700-1749          |Folio     |    14|          2.9|
|Leipzig    |Undetermined  |1700-1749          |Duodecimo |    14|          2.9|
|London     |English       |1700-1749          |Quarto    |  2286|         17.5|
|London     |English       |1700-1749          |Octavo    |  5776|         44.2|
|London     |English       |1700-1749          |Folio     |  4086|         31.3|
|London     |English       |1700-1749          |Duodecimo |   918|          7.0|
|London     |Latin         |1700-1749          |Quarto    |   213|         29.1|
|London     |Latin         |1700-1749          |Octavo    |   266|         36.4|
|London     |Latin         |1700-1749          |Folio     |   189|         25.9|
|London     |Latin         |1700-1749          |Duodecimo |    63|          8.6|
|London     |Undetermined  |1700-1749          |Quarto    |   551|         18.6|
|London     |Undetermined  |1700-1749          |Octavo    |  1280|         43.1|
|London     |Undetermined  |1700-1749          |Folio     |   896|         30.2|
|London     |Undetermined  |1700-1749          |Duodecimo |   243|          8.2|
|Madrid     |Latin         |1700-1749          |Quarto    |     2|         18.2|
|Madrid     |Latin         |1700-1749          |Octavo    |     3|         27.3|
|Madrid     |Latin         |1700-1749          |Folio     |     6|         54.5|
|Madrid     |Panjabi       |1700-1749          |Quarto    |    69|         53.5|
|Madrid     |Panjabi       |1700-1749          |Octavo    |    27|         20.9|
|Madrid     |Panjabi       |1700-1749          |Folio     |    31|         24.0|
|Madrid     |Panjabi       |1700-1749          |Duodecimo |     2|          1.6|
|Madrid     |Spanish       |1700-1749          |Quarto    |   235|         30.7|
|Madrid     |Spanish       |1700-1749          |Octavo    |    95|         12.4|
|Madrid     |Spanish       |1700-1749          |Folio     |   435|         56.8|
|Madrid     |Spanish       |1700-1749          |Duodecimo |     1|          0.1|
|Nürnberg   |German        |1700-1749          |Quarto    |   287|         21.6|
|Nürnberg   |German        |1700-1749          |Octavo    |   707|         53.2|
|Nürnberg   |German        |1700-1749          |Folio     |   266|         20.0|
|Nürnberg   |German        |1700-1749          |Duodecimo |    69|          5.2|
|Nürnberg   |Latin         |1700-1749          |Quarto    |   175|         38.4|
|Nürnberg   |Latin         |1700-1749          |Octavo    |   142|         31.1|
|Nürnberg   |Latin         |1700-1749          |Folio     |   122|         26.8|
|Nürnberg   |Latin         |1700-1749          |Duodecimo |    17|          3.7|
|Nürnberg   |Undetermined  |1700-1749          |Quarto    |    62|         55.9|
|Nürnberg   |Undetermined  |1700-1749          |Octavo    |    27|         24.3|
|Nürnberg   |Undetermined  |1700-1749          |Folio     |    10|          9.0|
|Nürnberg   |Undetermined  |1700-1749          |Duodecimo |    12|         10.8|
|Paris      |French        |1700-1749          |Quarto    |  3084|         42.1|
|Paris      |French        |1700-1749          |Octavo    |  1755|         24.0|
|Paris      |French        |1700-1749          |Folio     |   649|          8.9|
|Paris      |French        |1700-1749          |Duodecimo |  1833|         25.0|
|Paris      |Latin         |1700-1749          |Quarto    |   209|         27.2|
|Paris      |Latin         |1700-1749          |Octavo    |   119|         15.5|
|Paris      |Latin         |1700-1749          |Folio     |   372|         48.5|
|Paris      |Latin         |1700-1749          |Duodecimo |    67|          8.7|
|Paris      |Undetermined  |1700-1749          |Quarto    |   277|         31.9|
|Paris      |Undetermined  |1700-1749          |Octavo    |   168|         19.4|
|Paris      |Undetermined  |1700-1749          |Folio     |   150|         17.3|
|Paris      |Undetermined  |1700-1749          |Duodecimo |   272|         31.4|
|Prague     |Czech         |1700-1749          |Quarto    |     6|         16.2|
|Prague     |Czech         |1700-1749          |Octavo    |     7|         18.9|
|Prague     |Czech         |1700-1749          |Folio     |    21|         56.8|
|Prague     |Czech         |1700-1749          |Duodecimo |     3|          8.1|
|Prague     |German        |1700-1749          |Quarto    |    43|         15.8|
|Prague     |German        |1700-1749          |Octavo    |   111|         40.8|
|Prague     |German        |1700-1749          |Folio     |   112|         41.2|
|Prague     |German        |1700-1749          |Duodecimo |     6|          2.2|
|Prague     |Latin         |1700-1749          |Quarto    |    59|          9.5|
|Prague     |Latin         |1700-1749          |Octavo    |   162|         26.0|
|Prague     |Latin         |1700-1749          |Folio     |   395|         63.5|
|Prague     |Latin         |1700-1749          |Duodecimo |     6|          1.0|
|Rome       |Italian       |1700-1749          |Quarto    |   232|         50.0|
|Rome       |Italian       |1700-1749          |Octavo    |    77|         16.6|
|Rome       |Italian       |1700-1749          |Folio     |   140|         30.2|
|Rome       |Italian       |1700-1749          |Duodecimo |    15|          3.2|
|Rome       |Italian;Latin |1700-1749          |Quarto    |    10|         12.3|
|Rome       |Italian;Latin |1700-1749          |Octavo    |     1|          1.2|
|Rome       |Italian;Latin |1700-1749          |Folio     |    69|         85.2|
|Rome       |Italian;Latin |1700-1749          |Duodecimo |     1|          1.2|
|Rome       |Latin         |1700-1749          |Quarto    |   242|         25.6|
|Rome       |Latin         |1700-1749          |Octavo    |    69|          7.3|
|Rome       |Latin         |1700-1749          |Folio     |   626|         66.3|
|Rome       |Latin         |1700-1749          |Duodecimo |     7|          0.7|
|Stockholm  |Finnish       |1700-1749          |Quarto    |   174|         71.3|
|Stockholm  |Finnish       |1700-1749          |Octavo    |    26|         10.7|
|Stockholm  |Finnish       |1700-1749          |Folio     |     2|          0.8|
|Stockholm  |Finnish       |1700-1749          |Duodecimo |    42|         17.2|
|Stockholm  |Latin         |1700-1749          |Quarto    |   189|         37.3|
|Stockholm  |Latin         |1700-1749          |Octavo    |   172|         33.9|
|Stockholm  |Latin         |1700-1749          |Folio     |   131|         25.8|
|Stockholm  |Latin         |1700-1749          |Duodecimo |    15|          3.0|
|Stockholm  |Swedish       |1700-1749          |Quarto    |  3942|         69.2|
|Stockholm  |Swedish       |1700-1749          |Octavo    |   979|         17.2|
|Stockholm  |Swedish       |1700-1749          |Folio     |   557|          9.8|
|Stockholm  |Swedish       |1700-1749          |Duodecimo |   215|          3.8|
|Strasbourg |French        |1700-1749          |Quarto    |    26|         34.7|
|Strasbourg |French        |1700-1749          |Octavo    |    31|         41.3|
|Strasbourg |French        |1700-1749          |Folio     |    13|         17.3|
|Strasbourg |French        |1700-1749          |Duodecimo |     5|          6.7|
|Strasbourg |German        |1700-1749          |Quarto    |    28|         17.9|
|Strasbourg |German        |1700-1749          |Octavo    |    45|         28.8|
|Strasbourg |German        |1700-1749          |Folio     |    75|         48.1|
|Strasbourg |German        |1700-1749          |Duodecimo |     8|          5.1|
|Strasbourg |Latin         |1700-1749          |Quarto    |   481|         64.6|
|Strasbourg |Latin         |1700-1749          |Octavo    |    94|         12.6|
|Strasbourg |Latin         |1700-1749          |Folio     |   166|         22.3|
|Strasbourg |Latin         |1700-1749          |Duodecimo |     4|          0.5|
|The Hague  |French        |1700-1749          |Quarto    |   265|         17.2|
|The Hague  |French        |1700-1749          |Octavo    |   639|         41.5|
|The Hague  |French        |1700-1749          |Folio     |   104|          6.7|
|The Hague  |French        |1700-1749          |Duodecimo |   533|         34.6|
|The Hague  |Latin         |1700-1749          |Quarto    |   101|         42.1|
|The Hague  |Latin         |1700-1749          |Octavo    |    70|         29.2|
|The Hague  |Latin         |1700-1749          |Folio     |    65|         27.1|
|The Hague  |Latin         |1700-1749          |Duodecimo |     4|          1.7|
|The Hague  |Undetermined  |1700-1749          |Quarto    |    66|         41.0|
|The Hague  |Undetermined  |1700-1749          |Octavo    |    36|         22.4|
|The Hague  |Undetermined  |1700-1749          |Folio     |    33|         20.5|
|The Hague  |Undetermined  |1700-1749          |Duodecimo |    26|         16.1|
|Turku      |Finnish       |1700-1749          |Quarto    |    74|         36.5|
|Turku      |Finnish       |1700-1749          |Octavo    |    76|         37.4|
|Turku      |Finnish       |1700-1749          |Folio     |    13|          6.4|
|Turku      |Finnish       |1700-1749          |Duodecimo |    40|         19.7|
|Turku      |Latin         |1700-1749          |Quarto    |   386|         35.0|
|Turku      |Latin         |1700-1749          |Octavo    |   509|         46.2|
|Turku      |Latin         |1700-1749          |Folio     |   204|         18.5|
|Turku      |Latin         |1700-1749          |Duodecimo |     3|          0.3|
|Turku      |Swedish       |1700-1749          |Quarto    |   315|         42.2|
|Turku      |Swedish       |1700-1749          |Octavo    |    25|          3.3|
|Turku      |Swedish       |1700-1749          |Folio     |   403|         53.9|
|Turku      |Swedish       |1700-1749          |Duodecimo |     4|          0.5|
|Utrecht    |French        |1700-1749          |Quarto    |    16|          5.7|
|Utrecht    |French        |1700-1749          |Octavo    |   146|         51.6|
|Utrecht    |French        |1700-1749          |Folio     |     2|          0.7|
|Utrecht    |French        |1700-1749          |Duodecimo |   119|         42.0|
|Utrecht    |Latin         |1700-1749          |Quarto    |   340|         74.6|
|Utrecht    |Latin         |1700-1749          |Octavo    |   107|         23.5|
|Utrecht    |Latin         |1700-1749          |Folio     |     9|          2.0|
|Utrecht    |Undetermined  |1700-1749          |Quarto    |   135|         70.7|
|Utrecht    |Undetermined  |1700-1749          |Octavo    |    18|          9.4|
|Utrecht    |Undetermined  |1700-1749          |Folio     |    23|         12.0|
|Utrecht    |Undetermined  |1700-1749          |Duodecimo |    15|          7.9|
|Venice     |Italian       |1700-1749          |Quarto    |   340|         50.5|
|Venice     |Italian       |1700-1749          |Octavo    |   212|         31.5|
|Venice     |Italian       |1700-1749          |Folio     |    78|         11.6|
|Venice     |Italian       |1700-1749          |Duodecimo |    43|          6.4|
|Venice     |Latin         |1700-1749          |Quarto    |   374|         28.7|
|Venice     |Latin         |1700-1749          |Octavo    |    88|          6.8|
|Venice     |Latin         |1700-1749          |Folio     |   826|         63.4|
|Venice     |Latin         |1700-1749          |Duodecimo |    15|          1.2|
|Venice     |Undetermined  |1700-1749          |Quarto    |    18|         39.1|
|Venice     |Undetermined  |1700-1749          |Octavo    |    10|         21.7|
|Venice     |Undetermined  |1700-1749          |Folio     |    12|         26.1|
|Venice     |Undetermined  |1700-1749          |Duodecimo |     6|         13.0|
|Vienna     |German        |1700-1749          |Quarto    |    83|         35.8|
|Vienna     |German        |1700-1749          |Octavo    |    78|         33.6|
|Vienna     |German        |1700-1749          |Folio     |    64|         27.6|
|Vienna     |German        |1700-1749          |Duodecimo |     7|          3.0|
|Vienna     |Latin         |1700-1749          |Quarto    |    71|         23.5|
|Vienna     |Latin         |1700-1749          |Octavo    |    91|         30.1|
|Vienna     |Latin         |1700-1749          |Folio     |   126|         41.7|
|Vienna     |Latin         |1700-1749          |Duodecimo |    14|          4.6|
|Vienna     |Undetermined  |1700-1749          |Quarto    |    15|         20.5|
|Vienna     |Undetermined  |1700-1749          |Octavo    |    28|         38.4|
|Vienna     |Undetermined  |1700-1749          |Folio     |    16|         21.9|
|Vienna     |Undetermined  |1700-1749          |Duodecimo |    14|         19.2|
|Wittenberg |German        |1700-1749          |Quarto    |   126|         25.4|
|Wittenberg |German        |1700-1749          |Octavo    |   138|         27.8|
|Wittenberg |German        |1700-1749          |Folio     |   228|         46.0|
|Wittenberg |German        |1700-1749          |Duodecimo |     4|          0.8|
|Wittenberg |Latin         |1700-1749          |Quarto    |  2537|         84.8|
|Wittenberg |Latin         |1700-1749          |Octavo    |   366|         12.2|
|Wittenberg |Latin         |1700-1749          |Folio     |    83|          2.8|
|Wittenberg |Latin         |1700-1749          |Duodecimo |     6|          0.2|
|Wittenberg |Latin;German  |1700-1749          |Quarto    |   231|         88.2|
|Wittenberg |Latin;German  |1700-1749          |Octavo    |    11|          4.2|
|Wittenberg |Latin;German  |1700-1749          |Folio     |    19|          7.3|
|Wittenberg |Latin;German  |1700-1749          |Duodecimo |     1|          0.4|
|Amsterdam  |Dutch         |1750-1799          |Quarto    |   169|         26.1|
|Amsterdam  |Dutch         |1750-1799          |Octavo    |   408|         63.0|
|Amsterdam  |Dutch         |1750-1799          |Folio     |    50|          7.7|
|Amsterdam  |Dutch         |1750-1799          |Duodecimo |    21|          3.2|
|Amsterdam  |French        |1750-1799          |Quarto    |   310|         11.9|
|Amsterdam  |French        |1750-1799          |Octavo    |  1265|         48.6|
|Amsterdam  |French        |1750-1799          |Folio     |    59|          2.3|
|Amsterdam  |French        |1750-1799          |Duodecimo |   969|         37.2|
|Amsterdam  |Latin         |1750-1799          |Quarto    |    98|         45.6|
|Amsterdam  |Latin         |1750-1799          |Octavo    |    77|         35.8|
|Amsterdam  |Latin         |1750-1799          |Folio     |    32|         14.9|
|Amsterdam  |Latin         |1750-1799          |Duodecimo |     8|          3.7|
|Berlin     |French        |1750-1799          |Quarto    |   153|         16.1|
|Berlin     |French        |1750-1799          |Octavo    |   680|         71.6|
|Berlin     |French        |1750-1799          |Folio     |    22|          2.3|
|Berlin     |French        |1750-1799          |Duodecimo |    95|         10.0|
|Berlin     |German        |1750-1799          |Quarto    |   947|         12.3|
|Berlin     |German        |1750-1799          |Octavo    |  6071|         78.8|
|Berlin     |German        |1750-1799          |Folio     |   599|          7.8|
|Berlin     |German        |1750-1799          |Duodecimo |    88|          1.1|
|Berlin     |Latin         |1750-1799          |Quarto    |   110|         36.9|
|Berlin     |Latin         |1750-1799          |Octavo    |   167|         56.0|
|Berlin     |Latin         |1750-1799          |Folio     |    20|          6.7|
|Berlin     |Latin         |1750-1799          |Duodecimo |     1|          0.3|
|Brussels   |French        |1750-1799          |Quarto    |    92|         26.4|
|Brussels   |French        |1750-1799          |Octavo    |   149|         42.8|
|Brussels   |French        |1750-1799          |Folio     |    41|         11.8|
|Brussels   |French        |1750-1799          |Duodecimo |    66|         19.0|
|Brussels   |Latin         |1750-1799          |Quarto    |     4|         44.4|
|Brussels   |Latin         |1750-1799          |Octavo    |     3|         33.3|
|Brussels   |Latin         |1750-1799          |Folio     |     2|         22.2|
|Brussels   |Undetermined  |1750-1799          |Quarto    |   703|          6.9|
|Brussels   |Undetermined  |1750-1799          |Octavo    |  3600|         35.4|
|Brussels   |Undetermined  |1750-1799          |Folio     |  5472|         53.8|
|Brussels   |Undetermined  |1750-1799          |Duodecimo |   403|          4.0|
|Edinburgh  |English       |1750-1799          |Quarto    |   608|         31.0|
|Edinburgh  |English       |1750-1799          |Octavo    |   908|         46.4|
|Edinburgh  |English       |1750-1799          |Folio     |   164|          8.4|
|Edinburgh  |English       |1750-1799          |Duodecimo |   279|         14.2|
|Edinburgh  |Latin         |1750-1799          |Quarto    |    39|          3.9|
|Edinburgh  |Latin         |1750-1799          |Octavo    |   956|         94.9|
|Edinburgh  |Latin         |1750-1799          |Folio     |     4|          0.4|
|Edinburgh  |Latin         |1750-1799          |Duodecimo |     8|          0.8|
|Edinburgh  |Undetermined  |1750-1799          |Quarto    |    48|         19.8|
|Edinburgh  |Undetermined  |1750-1799          |Octavo    |   104|         42.8|
|Edinburgh  |Undetermined  |1750-1799          |Folio     |    54|         22.2|
|Edinburgh  |Undetermined  |1750-1799          |Duodecimo |    37|         15.2|
|Frankfurt  |German        |1750-1799          |Quarto    |   601|         11.8|
|Frankfurt  |German        |1750-1799          |Octavo    |  4340|         85.4|
|Frankfurt  |German        |1750-1799          |Folio     |   111|          2.2|
|Frankfurt  |German        |1750-1799          |Duodecimo |    27|          0.5|
|Frankfurt  |Latin         |1750-1799          |Quarto    |   336|         52.7|
|Frankfurt  |Latin         |1750-1799          |Octavo    |   275|         43.1|
|Frankfurt  |Latin         |1750-1799          |Folio     |    24|          3.8|
|Frankfurt  |Latin         |1750-1799          |Duodecimo |     3|          0.5|
|Frankfurt  |Undetermined  |1750-1799          |Quarto    |    63|         21.3|
|Frankfurt  |Undetermined  |1750-1799          |Octavo    |   209|         70.6|
|Frankfurt  |Undetermined  |1750-1799          |Folio     |    11|          3.7|
|Frankfurt  |Undetermined  |1750-1799          |Duodecimo |    13|          4.4|
|Göttingen  |German        |1750-1799          |Quarto    |   294|         16.5|
|Göttingen  |German        |1750-1799          |Octavo    |  1405|         78.6|
|Göttingen  |German        |1750-1799          |Folio     |    83|          4.6|
|Göttingen  |German        |1750-1799          |Duodecimo |     5|          0.3|
|Göttingen  |Latin         |1750-1799          |Quarto    |  1443|         59.7|
|Göttingen  |Latin         |1750-1799          |Octavo    |   757|         31.3|
|Göttingen  |Latin         |1750-1799          |Folio     |   214|          8.9|
|Göttingen  |Latin         |1750-1799          |Duodecimo |     4|          0.2|
|Göttingen  |Undetermined  |1750-1799          |Quarto    |    58|         41.7|
|Göttingen  |Undetermined  |1750-1799          |Octavo    |    71|         51.1|
|Göttingen  |Undetermined  |1750-1799          |Duodecimo |    10|          7.2|
|Halle      |German        |1750-1799          |Quarto    |   571|         17.0|
|Halle      |German        |1750-1799          |Octavo    |  2435|         72.6|
|Halle      |German        |1750-1799          |Folio     |   128|          3.8|
|Halle      |German        |1750-1799          |Duodecimo |   219|          6.5|
|Halle      |Latin         |1750-1799          |Quarto    |  1046|         75.1|
|Halle      |Latin         |1750-1799          |Octavo    |   308|         22.1|
|Halle      |Latin         |1750-1799          |Folio     |    26|          1.9|
|Halle      |Latin         |1750-1799          |Duodecimo |    12|          0.9|
|Halle      |Latin;German  |1750-1799          |Quarto    |    38|         62.3|
|Halle      |Latin;German  |1750-1799          |Octavo    |    21|         34.4|
|Halle      |Latin;German  |1750-1799          |Folio     |     2|          3.3|
|Hamburg    |French        |1750-1799          |Quarto    |    22|          6.7|
|Hamburg    |French        |1750-1799          |Octavo    |   261|         79.1|
|Hamburg    |French        |1750-1799          |Folio     |     3|          0.9|
|Hamburg    |French        |1750-1799          |Duodecimo |    44|         13.3|
|Hamburg    |German        |1750-1799          |Quarto    |   480|         16.2|
|Hamburg    |German        |1750-1799          |Octavo    |  2391|         80.7|
|Hamburg    |German        |1750-1799          |Folio     |    69|          2.3|
|Hamburg    |German        |1750-1799          |Duodecimo |    21|          0.7|
|Hamburg    |Latin         |1750-1799          |Quarto    |    53|         31.5|
|Hamburg    |Latin         |1750-1799          |Octavo    |    87|         51.8|
|Hamburg    |Latin         |1750-1799          |Folio     |    27|         16.1|
|Hamburg    |Latin         |1750-1799          |Duodecimo |     1|          0.6|
|Jena       |German        |1750-1799          |Quarto    |   170|         17.8|
|Jena       |German        |1750-1799          |Octavo    |   660|         69.1|
|Jena       |German        |1750-1799          |Folio     |   125|         13.1|
|Jena       |Latin         |1750-1799          |Quarto    |   359|         62.2|
|Jena       |Latin         |1750-1799          |Octavo    |   203|         35.2|
|Jena       |Latin         |1750-1799          |Folio     |    14|          2.4|
|Jena       |Latin         |1750-1799          |Duodecimo |     1|          0.2|
|Jena       |Latin;German  |1750-1799          |Quarto    |     5|         55.6|
|Jena       |Latin;German  |1750-1799          |Octavo    |     4|         44.4|
|Leiden     |French        |1750-1799          |Quarto    |    17|         14.3|
|Leiden     |French        |1750-1799          |Octavo    |    71|         59.7|
|Leiden     |French        |1750-1799          |Folio     |     1|          0.8|
|Leiden     |French        |1750-1799          |Duodecimo |    30|         25.2|
|Leiden     |Latin         |1750-1799          |Quarto    |   373|         62.0|
|Leiden     |Latin         |1750-1799          |Octavo    |   198|         32.9|
|Leiden     |Latin         |1750-1799          |Folio     |    27|          4.5|
|Leiden     |Latin         |1750-1799          |Duodecimo |     4|          0.7|
|Leiden     |Undetermined  |1750-1799          |Quarto    |    55|         39.3|
|Leiden     |Undetermined  |1750-1799          |Octavo    |    56|         40.0|
|Leiden     |Undetermined  |1750-1799          |Duodecimo |    29|         20.7|
|Leipzig    |German        |1750-1799          |Quarto    |  1066|         10.5|
|Leipzig    |German        |1750-1799          |Octavo    |  8818|         87.2|
|Leipzig    |German        |1750-1799          |Folio     |   182|          1.8|
|Leipzig    |German        |1750-1799          |Duodecimo |    44|          0.4|
|Leipzig    |Latin         |1750-1799          |Quarto    |  2199|         60.1|
|Leipzig    |Latin         |1750-1799          |Octavo    |  1349|         36.9|
|Leipzig    |Latin         |1750-1799          |Folio     |   100|          2.7|
|Leipzig    |Latin         |1750-1799          |Duodecimo |     9|          0.2|
|Leipzig    |Undetermined  |1750-1799          |Quarto    |   120|         24.2|
|Leipzig    |Undetermined  |1750-1799          |Octavo    |   310|         62.6|
|Leipzig    |Undetermined  |1750-1799          |Folio     |    31|          6.3|
|Leipzig    |Undetermined  |1750-1799          |Duodecimo |    34|          6.9|
|London     |English       |1750-1799          |Quarto    |  6029|         25.4|
|London     |English       |1750-1799          |Octavo    | 10646|         44.8|
|London     |English       |1750-1799          |Folio     |  5415|         22.8|
|London     |English       |1750-1799          |Duodecimo |  1678|          7.1|
|London     |Latin         |1750-1799          |Quarto    |   142|         36.9|
|London     |Latin         |1750-1799          |Octavo    |   173|         44.9|
|London     |Latin         |1750-1799          |Folio     |    50|         13.0|
|London     |Latin         |1750-1799          |Duodecimo |    20|          5.2|
|London     |Undetermined  |1750-1799          |Quarto    |   622|         12.6|
|London     |Undetermined  |1750-1799          |Octavo    |  2311|         46.9|
|London     |Undetermined  |1750-1799          |Folio     |  1348|         27.4|
|London     |Undetermined  |1750-1799          |Duodecimo |   644|         13.1|
|Madrid     |Latin         |1750-1799          |Quarto    |     8|         28.6|
|Madrid     |Latin         |1750-1799          |Folio     |    20|         71.4|
|Madrid     |Panjabi       |1750-1799          |Quarto    |   177|         33.2|
|Madrid     |Panjabi       |1750-1799          |Octavo    |   169|         31.7|
|Madrid     |Panjabi       |1750-1799          |Folio     |   180|         33.8|
|Madrid     |Panjabi       |1750-1799          |Duodecimo |     7|          1.3|
|Madrid     |Spanish       |1750-1799          |Quarto    |   498|         27.7|
|Madrid     |Spanish       |1750-1799          |Octavo    |   519|         28.8|
|Madrid     |Spanish       |1750-1799          |Folio     |   777|         43.1|
|Madrid     |Spanish       |1750-1799          |Duodecimo |     7|          0.4|
|Nürnberg   |German        |1750-1799          |Quarto    |   404|         24.4|
|Nürnberg   |German        |1750-1799          |Octavo    |  1057|         63.9|
|Nürnberg   |German        |1750-1799          |Folio     |   186|         11.2|
|Nürnberg   |German        |1750-1799          |Duodecimo |     7|          0.4|
|Nürnberg   |Latin         |1750-1799          |Quarto    |    69|         22.8|
|Nürnberg   |Latin         |1750-1799          |Octavo    |   184|         60.7|
|Nürnberg   |Latin         |1750-1799          |Folio     |    49|         16.2|
|Nürnberg   |Latin         |1750-1799          |Duodecimo |     1|          0.3|
|Nürnberg   |Undetermined  |1750-1799          |Quarto    |    45|         33.3|
|Nürnberg   |Undetermined  |1750-1799          |Octavo    |    74|         54.8|
|Nürnberg   |Undetermined  |1750-1799          |Folio     |     7|          5.2|
|Nürnberg   |Undetermined  |1750-1799          |Duodecimo |     9|          6.7|
|Paris      |French        |1750-1799          |Quarto    |  5371|         27.1|
|Paris      |French        |1750-1799          |Octavo    |  9350|         47.2|
|Paris      |French        |1750-1799          |Folio     |  1489|          7.5|
|Paris      |French        |1750-1799          |Duodecimo |  3585|         18.1|
|Paris      |Latin         |1750-1799          |Quarto    |   262|         45.3|
|Paris      |Latin         |1750-1799          |Octavo    |   138|         23.9|
|Paris      |Latin         |1750-1799          |Folio     |    49|          8.5|
|Paris      |Latin         |1750-1799          |Duodecimo |   129|         22.3|
|Paris      |Undetermined  |1750-1799          |Quarto    |   550|         18.1|
|Paris      |Undetermined  |1750-1799          |Octavo    |  1513|         49.8|
|Paris      |Undetermined  |1750-1799          |Folio     |   232|          7.6|
|Paris      |Undetermined  |1750-1799          |Duodecimo |   743|         24.5|
|Prague     |Czech         |1750-1799          |Quarto    |    37|         31.4|
|Prague     |Czech         |1750-1799          |Octavo    |    30|         25.4|
|Prague     |Czech         |1750-1799          |Folio     |    48|         40.7|
|Prague     |Czech         |1750-1799          |Duodecimo |     3|          2.5|
|Prague     |German        |1750-1799          |Quarto    |   160|         17.3|
|Prague     |German        |1750-1799          |Octavo    |   616|         66.5|
|Prague     |German        |1750-1799          |Folio     |   134|         14.5|
|Prague     |German        |1750-1799          |Duodecimo |    16|          1.7|
|Prague     |Latin         |1750-1799          |Quarto    |   163|         28.2|
|Prague     |Latin         |1750-1799          |Octavo    |   235|         40.7|
|Prague     |Latin         |1750-1799          |Folio     |   180|         31.1|
|Rome       |Italian       |1750-1799          |Quarto    |   193|         38.5|
|Rome       |Italian       |1750-1799          |Octavo    |   184|         36.7|
|Rome       |Italian       |1750-1799          |Folio     |    95|         19.0|
|Rome       |Italian       |1750-1799          |Duodecimo |    29|          5.8|
|Rome       |Italian;Latin |1750-1799          |Quarto    |     7|         63.6|
|Rome       |Italian;Latin |1750-1799          |Folio     |     4|         36.4|
|Rome       |Latin         |1750-1799          |Quarto    |   239|         36.5|
|Rome       |Latin         |1750-1799          |Octavo    |    92|         14.0|
|Rome       |Latin         |1750-1799          |Folio     |   321|         49.0|
|Rome       |Latin         |1750-1799          |Duodecimo |     3|          0.5|
|Stockholm  |Finnish       |1750-1799          |Quarto    |   478|         66.1|
|Stockholm  |Finnish       |1750-1799          |Octavo    |   126|         17.4|
|Stockholm  |Finnish       |1750-1799          |Folio     |     3|          0.4|
|Stockholm  |Finnish       |1750-1799          |Duodecimo |   116|         16.0|
|Stockholm  |Latin         |1750-1799          |Quarto    |    75|         29.5|
|Stockholm  |Latin         |1750-1799          |Octavo    |   149|         58.7|
|Stockholm  |Latin         |1750-1799          |Folio     |    22|          8.7|
|Stockholm  |Latin         |1750-1799          |Duodecimo |     8|          3.1|
|Stockholm  |Swedish       |1750-1799          |Quarto    |  8766|         60.1|
|Stockholm  |Swedish       |1750-1799          |Octavo    |  5006|         34.3|
|Stockholm  |Swedish       |1750-1799          |Folio     |   401|          2.8|
|Stockholm  |Swedish       |1750-1799          |Duodecimo |   407|          2.8|
|Strasbourg |French        |1750-1799          |Quarto    |   114|         36.1|
|Strasbourg |French        |1750-1799          |Octavo    |   170|         53.8|
|Strasbourg |French        |1750-1799          |Folio     |    26|          8.2|
|Strasbourg |French        |1750-1799          |Duodecimo |     6|          1.9|
|Strasbourg |German        |1750-1799          |Quarto    |    96|         17.6|
|Strasbourg |German        |1750-1799          |Octavo    |   348|         64.0|
|Strasbourg |German        |1750-1799          |Folio     |    88|         16.2|
|Strasbourg |German        |1750-1799          |Duodecimo |    12|          2.2|
|Strasbourg |Latin         |1750-1799          |Quarto    |   288|         55.8|
|Strasbourg |Latin         |1750-1799          |Octavo    |   145|         28.1|
|Strasbourg |Latin         |1750-1799          |Folio     |    80|         15.5|
|Strasbourg |Latin         |1750-1799          |Duodecimo |     3|          0.6|
|The Hague  |French        |1750-1799          |Quarto    |    83|         12.3|
|The Hague  |French        |1750-1799          |Octavo    |   438|         65.1|
|The Hague  |French        |1750-1799          |Folio     |    19|          2.8|
|The Hague  |French        |1750-1799          |Duodecimo |   133|         19.8|
|The Hague  |Latin         |1750-1799          |Quarto    |    31|         54.4|
|The Hague  |Latin         |1750-1799          |Octavo    |    17|         29.8|
|The Hague  |Latin         |1750-1799          |Folio     |     8|         14.0|
|The Hague  |Latin         |1750-1799          |Duodecimo |     1|          1.8|
|The Hague  |Undetermined  |1750-1799          |Quarto    |    25|         11.9|
|The Hague  |Undetermined  |1750-1799          |Octavo    |    96|         45.7|
|The Hague  |Undetermined  |1750-1799          |Folio     |    35|         16.7|
|The Hague  |Undetermined  |1750-1799          |Duodecimo |    54|         25.7|
|Turku      |Finnish       |1750-1799          |Quarto    |   153|         23.9|
|Turku      |Finnish       |1750-1799          |Octavo    |   364|         57.0|
|Turku      |Finnish       |1750-1799          |Folio     |    18|          2.8|
|Turku      |Finnish       |1750-1799          |Duodecimo |   104|         16.3|
|Turku      |Latin         |1750-1799          |Quarto    |  1311|         81.9|
|Turku      |Latin         |1750-1799          |Octavo    |    75|          4.7|
|Turku      |Latin         |1750-1799          |Folio     |   214|         13.4|
|Turku      |Latin         |1750-1799          |Duodecimo |     1|          0.1|
|Turku      |Swedish       |1750-1799          |Quarto    |  1847|         83.3|
|Turku      |Swedish       |1750-1799          |Octavo    |   190|          8.6|
|Turku      |Swedish       |1750-1799          |Folio     |   172|          7.8|
|Turku      |Swedish       |1750-1799          |Duodecimo |     8|          0.4|
|Utrecht    |French        |1750-1799          |Quarto    |    14|         32.6|
|Utrecht    |French        |1750-1799          |Octavo    |    18|         41.9|
|Utrecht    |French        |1750-1799          |Folio     |     1|          2.3|
|Utrecht    |French        |1750-1799          |Duodecimo |    10|         23.3|
|Utrecht    |Latin         |1750-1799          |Quarto    |    83|         59.3|
|Utrecht    |Latin         |1750-1799          |Octavo    |    55|         39.3|
|Utrecht    |Latin         |1750-1799          |Folio     |     2|          1.4|
|Utrecht    |Undetermined  |1750-1799          |Quarto    |    15|         16.3|
|Utrecht    |Undetermined  |1750-1799          |Octavo    |    30|         32.6|
|Utrecht    |Undetermined  |1750-1799          |Folio     |    32|         34.8|
|Utrecht    |Undetermined  |1750-1799          |Duodecimo |    15|         16.3|
|Venice     |Italian       |1750-1799          |Quarto    |   227|         25.3|
|Venice     |Italian       |1750-1799          |Octavo    |   572|         63.7|
|Venice     |Italian       |1750-1799          |Folio     |    38|          4.2|
|Venice     |Italian       |1750-1799          |Duodecimo |    61|          6.8|
|Venice     |Latin         |1750-1799          |Quarto    |   362|         38.3|
|Venice     |Latin         |1750-1799          |Octavo    |    96|         10.2|
|Venice     |Latin         |1750-1799          |Folio     |   471|         49.9|
|Venice     |Latin         |1750-1799          |Duodecimo |    15|          1.6|
|Venice     |Undetermined  |1750-1799          |Quarto    |    25|         45.5|
|Venice     |Undetermined  |1750-1799          |Octavo    |    13|         23.6|
|Venice     |Undetermined  |1750-1799          |Folio     |     6|         10.9|
|Venice     |Undetermined  |1750-1799          |Duodecimo |    11|         20.0|
|Vienna     |German        |1750-1799          |Quarto    |   646|         17.7|
|Vienna     |German        |1750-1799          |Octavo    |  2701|         73.8|
|Vienna     |German        |1750-1799          |Folio     |   175|          4.8|
|Vienna     |German        |1750-1799          |Duodecimo |   136|          3.7|
|Vienna     |Latin         |1750-1799          |Quarto    |   303|         29.5|
|Vienna     |Latin         |1750-1799          |Octavo    |   597|         58.2|
|Vienna     |Latin         |1750-1799          |Folio     |   118|         11.5|
|Vienna     |Latin         |1750-1799          |Duodecimo |     8|          0.8|
|Vienna     |Undetermined  |1750-1799          |Quarto    |    92|         19.6|
|Vienna     |Undetermined  |1750-1799          |Octavo    |   263|         56.0|
|Vienna     |Undetermined  |1750-1799          |Folio     |    98|         20.9|
|Vienna     |Undetermined  |1750-1799          |Duodecimo |    17|          3.6|
|Wittenberg |German        |1750-1799          |Quarto    |    50|         21.3|
|Wittenberg |German        |1750-1799          |Octavo    |   141|         60.0|
|Wittenberg |German        |1750-1799          |Folio     |    44|         18.7|
|Wittenberg |Latin         |1750-1799          |Quarto    |   833|         86.3|
|Wittenberg |Latin         |1750-1799          |Octavo    |   119|         12.3|
|Wittenberg |Latin         |1750-1799          |Folio     |    13|          1.3|
|Wittenberg |Latin;German  |1750-1799          |Quarto    |    62|         98.4|
|Wittenberg |Latin;German  |1750-1799          |Folio     |     1|          1.6|



## Top 100 publication places

Top publication places for the total number of books in
1700-1799. **Missing places have been omitted in percentage
calculations.**


| Rank|City            | Entries (N)| Fraction (%)|
|----:|:---------------|-----------:|------------:|
|    1|Paris           |      119440|         12.0|
|    2|London          |      116751|         11.7|
|    3|Leipzig         |       47299|          4.7|
|    4|Amsterdam       |       40510|          4.1|
|    5|Stockholm       |       25274|          2.5|
|    6|Frankfurt       |       22081|          2.2|
|    7|Halle           |       20967|          2.1|
|    8|Berlin          |       20226|          2.0|
|    9|Brussels        |       15645|          1.6|
|   10|Madrid          |       13895|          1.4|
|   11|The Hague       |       13866|          1.4|
|   12|Leiden          |       12379|          1.2|
|   13|Vienna          |       12200|          1.2|
|   14|Prague          |       11313|          1.1|
|   15|Hamburg         |       10763|          1.1|
|   16|Göttingen       |       10156|          1.0|
|   17|Venice          |        9841|          1.0|
|   18|Nürnberg        |        9173|          0.9|
|   19|Jena            |        8860|          0.9|
|   20|Wittenberg      |        8559|          0.9|
|   21|Rome            |        8506|          0.9|
|   22|Utrecht         |        8428|          0.8|
|   23|Edinburgh       |        8187|          0.8|
|   24|Strasbourg      |        7762|          0.8|
|   25|Turku           |        7157|          0.7|
|   26|Helmstedt       |        6730|          0.7|
|   27|Augsburg        |        6212|          0.6|
|   28|Lyon            |        5628|          0.6|
|   29|Uppsala         |        5604|          0.6|
|   30|Dublin          |        5175|          0.5|
|   31|Geneva          |        5011|          0.5|
|   32|St Petersburg   |        4848|          0.5|
|   33|Tübingen        |        4495|          0.5|
|   34|Dresden         |        4199|          0.4|
|   35|Rostock         |        4018|          0.4|
|   36|Cologne         |        3908|          0.4|
|   37|Rotterdam       |        3786|          0.4|
|   38|Oxford          |        3380|          0.3|
|   39|Hannover        |        3272|          0.3|
|   40|Augusta         |        3234|          0.3|
|   41|Copenhagen      |        3207|          0.3|
|   42|Philadelphia Pa |        3065|          0.3|
|   43|Boston Ma       |        3048|          0.3|
|   44|Greifswald      |        3006|          0.3|
|   45|Braunschweig    |        2890|          0.3|
|   46|Zürich          |        2847|          0.3|
|   47|Basel           |        2795|          0.3|
|   48|Gotha           |        2544|          0.3|
|   49|Naples          |        2371|          0.2|
|   50|Breslau         |        2285|          0.2|
|   51|Lund            |        2281|          0.2|
|   52|Halae           |        2241|          0.2|
|   53|Moscow          |        2210|          0.2|
|   54|Toulouse        |        2018|          0.2|
|   55|Groningen       |        2010|          0.2|
|   56|Erlangen        |        2010|          0.2|
|   57|Erfurt          |        1973|          0.2|
|   58|Riga            |        1962|          0.2|
|   59|Altdorf         |        1929|          0.2|
|   60|Gießen          |        1875|          0.2|
|   61|Bremen          |        1845|          0.2|
|   62|Liège           |        1794|          0.2|
|   63|München         |        1791|          0.2|
|   64|Regensburg      |        1776|          0.2|
|   65|Magdeburg       |        1776|          0.2|
|   66|Avignon         |        1773|          0.2|
|   67|Vaasa           |        1771|          0.2|
|   68|Ulm             |        1615|          0.2|
|   69|Weimar          |        1598|          0.2|
|   70|Marburg         |        1570|          0.2|
|   71|Antwerp         |        1530|          0.2|
|   72|Konigsberg      |        1498|          0.2|
|   73|Lübeck          |        1476|          0.1|
|   74|Rouen           |        1447|          0.1|
|   75|Padua           |        1415|          0.1|
|   76|Lisbon          |        1403|          0.1|
|   77|Lausanne        |        1384|          0.1|
|   78|Erfordiae       |        1382|          0.1|
|   79|Valencia        |        1374|          0.1|
|   80|Montpellier     |        1371|          0.1|
|   81|Milan           |        1358|          0.1|
|   82|Altona          |        1358|          0.1|
|   83|Cambridge       |        1326|          0.1|
|   84|Glasgow         |        1313|          0.1|
|   85|Firenze         |        1294|          0.1|
|   86|Regiomonti      |        1278|          0.1|
|   87|Bologna         |        1270|          0.1|
|   88|Kiel            |        1266|          0.1|
|   89|Gdańsk          |        1264|          0.1|
|   90|Haarlem         |        1257|          0.1|
|   91|Schwerin        |        1240|          0.1|
|   92|Bern            |        1211|          0.1|
|   93|Barcelona       |        1196|          0.1|
|   94|Matriti         |        1135|          0.1|
|   95|Gothenburg      |        1103|          0.1|
|   96|Lemgo           |        1099|          0.1|
|   97|Mannheim        |        1074|          0.1|
|   98|Stuttgart       |        1067|          0.1|
|   99|Altenburg       |        1035|          0.1|
|  100|Monspelii       |        1029|          0.1|
