df <- df0
df$gatherings <- as.character(df$gatherings)
df$gatherings[which(!df$gatherings %in% top.gatherings)] <- "Other"
df$gatherings <- factor(df$gatherings, levels = c(top.gatherings, "Other"))
df <- df %>%  filter(!is.na(gatherings)) %>%
              filter(publication_place %in% cities) %>%
              group_by(publication_year, publication_place, gatherings) %>%
	      summarise(n = n()) %>%
	      filter(n>0) %>% 	      
	      arrange(publication_year)

p <- ggplot(df, aes(x = publication_year, y = n,
                    color = gatherings)) +
		    geom_point() +
       		    my_color_scale() +
       guides(color=guide_legend(title="Gatherings"), fill=FALSE)    

library(cowplot)
legend <- get_legend(p)

# Cross plot on publication year vs. page counts in different languages
pics <- list()
for (place in sort(unique(df$publication_place))) {
  dfs <- df %>% filter(publication_place == place)
  p <- ggplot(dfs, aes(x = publication_year, y = n,
                    color = gatherings)) +
       scale_y_log10(limits = range(dfs$n), breaks = 10^(0:3)) + 
       geom_point() +
       geom_smooth() +
       my_color_scale() +       
       labs(x = "Publication year", y = "Titles (n)", title = place) +
       guides(color=FALSE, fill=FALSE)   
  pics[[place]] <- p
}

prow <-  plot_grid(
  pics[[1]], pics[[2]], pics[[3]], pics[[4]], pics[[5]],
  pics[[6]], pics[[7]], pics[[8]], pics[[9]], pics[[10]],
  pics[[11]], pics[[12]], 
  nrow = 4)
plot_grid(prow, legend, rel_widths = c(20, 5))
