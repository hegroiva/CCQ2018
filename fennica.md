---
title: "CCQ 2018 Figures and Tables: Fennica"
author: "Lahti, Marjanen, Roivainen, Tolonen"
date: "2018-06-29"
output: md_document
---

# CCQ 2018 / Fennica



Multilingual documents that include Swedish and Latin are listed in
the file [multilingual.csv](multilingual.csv).

<img src="figures/fennica_multilingual-1.png" title="plot of chunk multilingual" alt="plot of chunk multilingual" width="500px" />






## General 1: Latin share

Annual title count percentage of books in Latin per year (missing
mappings ignored). 

<img src="figures/fennica_latin_share-1.png" title="plot of chunk latin_share" alt="plot of chunk latin_share" width="500px" />




## Vernacularization

Paper consumption in the top languages in the whole catalog.

![plot of chunk vernacularization_overall](figures/fennica_vernacularization_overall-1.png)


## Vernacularizations by city

Annual paper consumption for different (primary) languages in selected places.

<img src="figures/fennica_vernacular_by_city-1.png" title="plot of chunk vernacular_by_city" alt="plot of chunk vernacular_by_city" width="500px" />



## Octavo 

Annual paper consumption for different book formats: total and relative.

<img src="figures/fennica_octavo-1.png" title="plot of chunk octavo" alt="plot of chunk octavo" width="550px" /><img src="figures/fennica_octavo-2.png" title="plot of chunk octavo" alt="plot of chunk octavo" width="550px" />

